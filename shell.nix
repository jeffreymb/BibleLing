with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "node";
    buildInputs = [
        mysql80
        yarn
        nodejs-16_x
    ];
    shellHook = ''
        export PATH="$PWD/node_modules/.bin/:$PATH"
    '';
}

