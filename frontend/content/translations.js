// window.appConfig.translations = {
//     en: {
//         language_name: 'English',
//         welcome_to: 'Welcome to {lang_name}!',
//         next: 'Next',
//         previous: 'Previous',
//         main_menu: 'Main Menu',
//         settings: {
//             title: 'Settings',
//             enable_feedback_sounds: 'Enable Feedback Sounds',
//         },
//         correct: 'Correct',
//         incorrect: 'Incorrect',
//         photo_credits: 'Photo Credits',
//         page_x_of_y: 'Page {0} of {1}',
//     },
//     sw: {
//         language_name: 'Kiswahili',
//         welcome_to: 'Karibu {lang_name}!',
//         next: "Mbele",
//         previous: "Nyuma",
//         main_menu: 'Menu Kuu',
//         settings: {
//             title: 'Mipangilio',
//             enable_feedback_sounds: 'Piga sauti kwenye mtihani',
//         },
//         correct: 'Sahihi',
//         incorrect: 'Makosa',
//         photo_credits: 'Chanzo cha Mapicha',
//         page_x_of_y: 'Sehemu {0} katika {1}',
//     }
// };