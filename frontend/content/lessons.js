// window.appConfig.lessons = [
//     {
//         "title": "Lesson 1",
//         "sections": [
//             {
//                 "type": "discovery",
//                 "title": "Click and Listen",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "image": "man_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "woman_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "boy_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "girl_1",
//                                 "audio": true,
//                                 "id": 3
//                             }
//                         ],
//                         "note": "People Nouns"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "bull_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "cow_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "camel_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "sheep_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "dog_1",
//                                 "audio": true,
//                                 "id": 4
//                             }
//                         ],
//                         "note": "Animal Nouns"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "walking_camel_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "walking_sheep_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "walking_dog_1",
//                                 "audio": true,
//                                 "id": 2
//                             }
//                         ],
//                         "note": "masculine first verb"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "eating_man_2",
//                                 "audio": "eating_man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "eating_boy_2",
//                                 "audio": "eating_boy_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "eating_bull_2",
//                                 "audio": "eating_bull_1",
//                                 "id": 2
//                             }
//                         ],
//                         "note": "masculine second verb"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "eating_woman_2",
//                                 "audio": "eating_woman_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "eating_girl_2",
//                                 "audio": "eating_girl_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "eating_cow_1",
//                                 "audio": "eating_cow_1",
//                                 "id": 2
//                             }
//                         ],
//                         "note": "feminine second verb"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "walking_man_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "walking_boy_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "walking_bull_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "eating_sheep_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "eating_dog_1",
//                                 "audio": true,
//                                 "id": 4
//                             },
//                             {
//                                 "image": "eating_camel_1",
//                                 "audio": true,
//                                 "id": 5
//                             }
//                         ],
//                         "note": "all masculine, contrasting the 2 verbs"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "eating_man_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "walking_boy_2",
//                                 "audio": "walking_boy_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "eating_bull_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "eating_woman_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "walking_girl_1",
//                                 "audio": true,
//                                 "id": 4
//                             },
//                             {
//                                 "image": "eating_cow_2",
//                                 "audio": "eating_cow_1",
//                                 "id": 5
//                             }
//                         ],
//                         "note": "contrasing masc and fem for both verbs"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "eating_man_2",
//                                 "audio": "eating_man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "eating_boy_2",
//                                 "audio": "eating_boy_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "eating_bull_2",
//                                 "audio": "eating_bull_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "walking_woman_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "walking_girl_2",
//                                 "audio": "walking_girl_1",
//                                 "id": 4
//                             },
//                             {
//                                 "image": "walking_cow_1",
//                                 "audio": true,
//                                 "id": 5
//                             }
//                         ],
//                         "note": "contrasting feminine first verb and masculine second verb"
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "eating_woman_2",
//                                 "audio": "eating_woman_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "eating_girl_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "eating_cow_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "walking_man_2",
//                                 "audio": "walking_man_1",
//                                 "id": 3
//                             },
//                             {
//                                 "image": "walking_boy_1",
//                                 "audio": true,
//                                 "id": 4
//                             },
//                             {
//                                 "image": "walking_bull_2",
//                                 "audio": "walking_bull_1",
//                                 "id": 5
//                             }
//                         ],
//                         "note": "contrasting feminine second verb and masculine first verb"
//                     }
//                 ],
//                 "qtyContent": 42
//             },
//             {
//                 "type": "quiz",
//                 "title": "Quiz 1",
//                 "maxShown": 6,
//                 "minShown": 3,
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "image": "man_2",
//                                 "audio": "man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "woman_2",
//                                 "audio": "woman_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "boy_2",
//                                 "audio": "boy_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "girl_2",
//                                 "audio": "girl_1",
//                                 "id": 3
//                             },
//                             {
//                                 "image": "bull_2",
//                                 "audio": "bull_1",
//                                 "id": 4
//                             },
//                             {
//                                 "image": "cow_2",
//                                 "audio": "cow_1",
//                                 "id": 5
//                             },
//                             {
//                                 "image": "camel_2",
//                                 "audio": "camel_1",
//                                 "id": 6
//                             },
//                             {
//                                 "image": "sheep_2",
//                                 "audio": "sheep_1",
//                                 "id": 7
//                             },
//                             {
//                                 "image": "dog_2",
//                                 "audio": "dog_1",
//                                 "id": 8
//                             }
//                         ],
//                         "random": false
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "walking_camel_2",
//                                 "audio": "walking_camel_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "walking_sheep_2",
//                                 "audio": "walking_sheep_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "walking_dog_2",
//                                 "audio": "walking_dog_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "eating_man_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "eating_boy_1",
//                                 "audio": true,
//                                 "id": 4
//                             },
//                             {
//                                 "image": "eating_bull_1",
//                                 "audio": true,
//                                 "id": 5
//                             },
//                             {
//                                 "image": "eating_woman_1",
//                                 "audio": true,
//                                 "id": 6
//                             },
//                             {
//                                 "image": "eating_girl_1",
//                                 "audio": true,
//                                 "id": 7
//                             },
//                             {
//                                 "image": "eating_cow_1",
//                                 "audio": true,
//                                 "id": 8
//                             },
//                             {
//                                 "image": "walking_man_2",
//                                 "audio": "walking_man_1",
//                                 "id": 9
//                             },
//                             {
//                                 "image": "eating_dog_2",
//                                 "audio": "eating_dog_1",
//                                 "id": 10
//                             },
//                             {
//                                 "image": "eating_camel_1",
//                                 "audio": true,
//                                 "id": 11
//                             }
//                         ],
//                         "random": true
//                     }
//                 ],
//                 "qtyContent": 21
//             }
//         ]
//     },
//     {
//         "title": "Dev Testing",
//         "sections": [
//             {
//                 "type": "discovery",
//                 "title": "Click and Listen",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "image": "man_2",
//                                 "audio": "man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "woman_2",
//                                 "audio": "woman_1",
//                                 "id": 1
//                             }
//                         ]
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "bull_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "cow_1",
//                                 "audio": true,
//                                 "id": 1
//                             },
//                             {
//                                 "image": "camel_1",
//                                 "audio": true,
//                                 "id": 2
//                             },
//                             {
//                                 "image": "sheep_1",
//                                 "audio": true,
//                                 "id": 3
//                             },
//                             {
//                                 "image": "dog_1",
//                                 "audio": true,
//                                 "id": 4
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 7
//             },
//             {
//                 "type": "quiz",
//                 "title": "Quiz - small",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "image": "man_2",
//                                 "audio": "man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "woman_2",
//                                 "audio": "woman_1",
//                                 "id": 1
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 2
//             },
//             {
//                 "type": "quiz",
//                 "title": "Quiz - paged",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "image": "man_2",
//                                 "audio": "man_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "woman_2",
//                                 "audio": "woman_1",
//                                 "id": 1
//                             }
//                         ]
//                     },
//                     {
//                         "content": [
//                             {
//                                 "image": "boy_2",
//                                 "audio": "boy_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "girl_2",
//                                 "audio": "girl_1",
//                                 "id": 1
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 4
//             },
//             {
//                 "type": "read_select_image",
//                 "title": "Read and Select Image",
//                 "pages": [
//                     {
//                         "text": "הָאִשָּׁה רֹכֶבֶת עַל חֲמוֹר",
//                         "content": [
//                             {
//                                 "image": "riding_woman_on_donkey_1",
//                                 "audio": "woman_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "riding_girl_on_horse_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "riding_man_on_horse_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "riding_boy_on_camel_1",
//                                 "id": 3
//                             }
//                         ]
//                     },
//                     {
//                         "text": "Page 2 text",
//                         "content": [
//                             {
//                                 "image": "girl_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "image": "boy_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "man_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "woman_1",
//                                 "id": 3
//                             }
//                         ]
//                     },
//                     {
//                         "text": "This one has no audio",
//                         "content": [
//                             {
//                                 "image": "bull_1",
//                                 "id": 0
//                             },
//                             {
//                                 "image": "cow_1",
//                                 "id": 1
//                             },
//                             {
//                                 "image": "camel_1",
//                                 "id": 2
//                             },
//                             {
//                                 "image": "sheep_1",
//                                 "id": 3
//                             },
//                             {
//                                 "image": "dog_1",
//                                 "id": 4
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 13
//             },
//             {
//                 "type": "image_select_text",
//                 "title": "See Image and Select Text",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "text": "הָאִשָּׁה רֹכֶבֶת עַל חֲמוֹר",
//                                 "image": "riding_woman_on_donkey_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "text": "הָאִשָּׁה רֹכֶבֶת עַל סוּס",
//                                 "audio": "riding_woman_on_horse_1",
//                                 "id": 1
//                             },
//                             {
//                                 "text": "הָאִישׁ רֹכֵב עַל גָּמַל",
//                                 "audio": "riding_man_on_camel_1",
//                                 "id": 2
//                             }
//                         ]
//                     },
//                     {
//                         "content": [
//                             {
//                                 "text": "this is the correct answer",
//                                 "image": "woman_1",
//                                 "audio": true,
//                                 "id": 0
//                             },
//                             {
//                                 "text": "wrong 1",
//                                 "image": "walking_camel_1",
//                                 "id": 1
//                             },
//                             {
//                                 "text": "wrong 2",
//                                 "image": "eating_boy_2",
//                                 "audio": "boy_1",
//                                 "id": 2
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 6
//             },
//             {
//                 "type": "match_image_text",
//                 "title": "Match Image & Text",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "text": "boy",
//                                 "image": "boy_1",
//                                 "id": 0
//                             },
//                             {
//                                 "text": "girl with a really long text to test things out.",
//                                 "image": "girl_1",
//                                 "id": 1
//                             },
//                             {
//                                 "text": "cow",
//                                 "image": "cow_1",
//                                 "id": 2
//                             }
//                         ]
//                     },
//                     {
//                         "content": [
//                             {
//                                 "text": "הַיַּלְדָּה רֹכֶבֶת עַל סוּס",
//                                 "image": "riding_girl_on_horse_1",
//                                 "id": 0
//                             },
//                             {
//                                 "text": "הָאִישׁ רֹכֵב עַל סוּס",
//                                 "image": "riding_man_on_horse_1",
//                                 "id": 1
//                             },
//                             {
//                                 "text": "הַיֶּלֶד רֹכֵב עַל גָּמַל",
//                                 "image": "riding_boy_on_camel_1",
//                                 "id": 2
//                             },
//                             {
//                                 "text": "הָאִשָּׁה רֹכֶבֶת עַל חֲמוֹר",
//                                 "image": "riding_woman_on_donkey_1",
//                                 "id": 3
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 7
//             },
//             {
//                 "type": "text",
//                 "title": "Text",
//                 "pages": [
//                     {
//                         "content": [
//                             {
//                                 "html": "<h1 class=\"ql-indent-1\">Header</h1><p class=\"ql-indent-1\">2</p><p class=\"ql-indent-2\">3</p><p><br></p><p class=\"ql-align-center\"><span class=\"ql-cursor\">﻿﻿﻿﻿﻿</span><img src=\"images/walking_camel_1.jpg\" width=\"323\" height=\"270\"></p><p class=\"ql-align-center\"><br></p><p>Some other random text</p><p class=\"ql-align-center\"><br></p><p class=\"ql-direction-rtl ql-align-right\"><span class=\"ql-size-large ql-font-serif\">הָאִשָּׁה רֹכֶבֶת עַל חֲמוֹר</span></p>",
//                                 "id": 0
//                             }
//                         ]
//                     },
//                     {
//                         "content": [
//                             {
//                                 "html": "<h1>Page 2</h1><ol><li>one</li><li>two</li><li>three</li><li><br></li></ol>",
//                                 "id": 0
//                             }
//                         ]
//                     }
//                 ],
//                 "qtyContent": 2
//             }
//         ]
//     }
// ];