const webpack = require('webpack'),
  path = require('path'),
  MiniCssExtractPlugin = require('mini-css-extract-plugin'),
  TerserJsPlugin = require('terser-webpack-plugin'),
  OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
  CleanWebPackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin,
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  CspHtmlWebpackPlugin = require('csp-html-webpack-plugin'),
  // HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin'),
  VueLoaderPlugin = require('vue-loader/lib/plugin'),
  // CopyWebpackPlugin = require('copy-webpack-plugin'),
  LodashModuleReplacementPlugin = require('lodash-webpack-plugin'),
  RollbarSourceMapPlugin = require('rollbar-sourcemap-webpack-plugin'),
  BundleAnalyazerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
  SpeedMeasurePlugin = require('speed-measure-webpack-plugin'),
  _ = require('lodash'),
  fs = require('fs'),
  et = require('elementtree');

module.exports = function (env, options) {
  const isProdWeb = !!(options.mode === 'production' && process.env.CI_COMMIT_SHA),
    isMinimalDev = env !== undefined ? env.minimal === 'true' : false,
    isMobileApp = env !== undefined ? env.mobile === 'true' : false,
    enableAdmin = env !== undefined ? env.admin === 'true' : false,
    rollbarAccessToken = isProdWeb ? process.env.ROLLBAR_TOKEN : '',
    rollbarPostToken = isProdWeb ? process.env.ROLLBAR_SERVER_TOKEN : '',
    codeVersion = isProdWeb ? process.env.CI_COMMIT_SHA : 'dev',
    NullPlugin = function () {
      this.apply = function () {
        // noop
      };
    };
  let mobileConfig = {},
    mobileDomain = '',
    mobileSiteConfig = {},
    cspConfig = {
      'base-uri': ["'self'"],
      'default-src': ["'self'"],
      'script-src': ["'self'", "'unsafe-eval'"],
      'style-src': ["'self'", "'unsafe-inline'"],
      'object-src': ["'self'"],
      'img-src': ["'self'"],
    },
    cspEnabled = false;
  if (options.hot) {
    // Disable CSP for HMR.
    cspEnabled = false;
  }

  const entry = {};

  if (!isMobileApp) {
    console.log('isProdWeb:', isProdWeb);

    entry['ClientMain'] = ['babel-polyfill', 'ClientMain.js'];
    // Allow GA.
    cspConfig['script-src'].push('https://www.google-analytics.com/analytics.js');
    cspConfig['img-src'].push('https://www.google-analytics.com');
  } else {
    // Polyfill needed for async/await functionality and I don't know what all else.
    entry['MobileMain'] = ['babel-polyfill', 'MobileMain.js'];

    mobileConfig = require('../mobile/mobile.config.json');

    if (
      env['mobile-config'] !== undefined &&
      mobileConfig.apps[env['mobile-config']] !== undefined
    ) {
      mobileSiteConfig = mobileConfig.apps[env['mobile-config']];
      mobileDomain = mobileSiteConfig.domain;
      delete mobileConfig.apps;
    } else {
      throw new Error(
        `Missing or invalid mobile-config config, got "${env['mobile-config']}". Please verify your mobile.config.json.`
      );
    }

    if (options.hot) {
      mobileDomain = 'http://localhost:3001';
    }

    cspConfig['default-src'].push(mobileDomain);
    cspConfig['default-src'].push('cdvfile:');
    cspConfig['img-src'].push(mobileDomain);
    cspConfig['img-src'].push('data:');
    cspConfig['img-src'].push('cdvfile:');
    // So the CAPTCHA will work on the contact page.
    cspConfig['object-src'].push(mobileDomain);

    console.log(`Building mobile app for "${mobileDomain}".`);

    const configPath = path.join(__dirname, '../mobile/config.xml'),
      data = fs.readFileSync(configPath).toString(),
      tree = et.parse(data),
      root = tree.getroot(),
      SubElement = et.SubElement;
    root.set('id', mobileSiteConfig.appId);
    root.set('version', mobileConfig.version);

    root.findall('./description')[0].text = mobileSiteConfig.description;
    root.findall('./name')[0].text = mobileSiteConfig.name;
    const author = root.findall('./author')[0];

    author.text = mobileSiteConfig.author.name;
    author.set('email', mobileSiteConfig.author.email);
    author.set('href', mobileSiteConfig.author.url);

    // Find and remove all previous access configurations.
    const prevAccess = root.findall('./access');
    for (const a in prevAccess) {
      root.remove(prevAccess[a]);
    }
    const access = SubElement(root, 'access');
    access.set('origin', mobileDomain);

    const content = root.findall('./content')[0];

    if (options.hot) {
      const accessDev = SubElement(root, 'access');
      accessDev.set('origin', '*');

      content.set('src', 'http://localhost:8081/index.html');
    } else {
      const accessCdv = SubElement(root, 'access');
      accessCdv.set('origin', 'cdvfile://*');

      content.set('src', 'index.html');
    }

    const icons = root.findall('./icon');
    for (const i in icons) {
      root.remove(icons[i]);
    }
    if (mobileSiteConfig.icons && mobileSiteConfig.icons.length > 0) {
      for (let i = 0; i < mobileSiteConfig.icons.length; i++) {
        const row = mobileSiteConfig.icons[i],
          sourcePath = path.join(__dirname, row.src);
        fs.copyFileSync(
          sourcePath,
          path.join(__dirname, '../mobile/res/', path.basename(sourcePath))
        );
        const icon = SubElement(root, 'icon');
        icon.set('src', 'res/' + path.basename(sourcePath));
      }
    }

    fs.writeFileSync(
      configPath,
      tree.write({
        indent: 4,
        xml_declaration: true,
      })
    );
    console.log('Updated ../mobile/config.xml');

    if (mobileSiteConfig.build) {
      fs.writeFileSync(
        path.join(__dirname, '../mobile/build.json'),
        JSON.stringify(mobileSiteConfig.build, undefined, 4)
      );
      console.log('Wrote ../mobile/build.json');
    }
  }

  const output = {
    path: path.resolve(__dirname, '../backend/dist/public'),
    filename: options.hot || isMobileApp ? '[name].js' : '[name].[contenthash].js',
  };

  const plugins = [
    new HtmlWebpackPlugin({
      title: isMobileApp ? mobileSiteConfig.name : 'Loading...',
      filename: 'index.html',
      template: isMobileApp ? 'src/templateMobile.html' : 'src/template.html',
      excludeChunks: ['AdminMain'],
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
    }),
    new CspHtmlWebpackPlugin(cspConfig, {
      enabled: cspEnabled,
    }),
    new webpack.DefinePlugin({
      PRODUCTION: isProdWeb,
      ROLLBAR_TOKEN: JSON.stringify(rollbarAccessToken),
      VERSION: JSON.stringify(codeVersion),
      IS_MOBILE: isMobileApp,
      MOBILE_DOMAIN: JSON.stringify(mobileDomain),
    }),
    new webpack.HashedModuleIdsPlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: options.hot || isMobileApp ? '[name].css' : '[name].[contenthash].css',
      chunkFilename: options.hot || isMobileApp ? '[name].css' : '[name].[contenthash].css',
    }),
    new LodashModuleReplacementPlugin(),
    !isProdWeb || isMobileApp
      ? new NullPlugin()
      : new RollbarSourceMapPlugin({
          accessToken: rollbarPostToken,
          version: codeVersion,
          publicPath: 'https://hebrew.bibleling.org',
        }),
    // new BundleAnalyazerPlugin({}),
    // new SpeedMeasurePlugin(),
  ];

  if (isMobileApp) {
    output.path = path.resolve(__dirname, '../mobile/www');
    // plugins.push(new CleanWebPackPlugin([path.join(output.path, '*')], {
    //     allowExternal: true,
    //     exclude: ['gitkeep']
    // }));
  } else {
    // Replace all mobile modules with the null module.
    plugins.push(new webpack.NormalModuleReplacementPlugin(/.*\/mobile\/.*/, 'lib/NullModule'));
  }

  if (enableAdmin) {
    entry['AdminMain'] = ['babel-polyfill', 'AdminMain.js'];

    let adminCspConfig = _.cloneDeep(cspConfig);

    adminCspConfig['img-src'].push('https://upload.wikimedia.org/');
    adminCspConfig['media-src'] = adminCspConfig['img-src'];

    plugins.unshift(
      new HtmlWebpackPlugin({
        title: 'Language Learning Admin App',
        filename: 'admin.html',
        template: 'src/template.html',
        excludeChunks: ['ClientMain'],
        meta: {
          viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        },
        cspPlugin: {
          enabled: cspEnabled,
          policy: adminCspConfig,
        },
      })
    );
  }

  const stats = {};

  if (!isMobileApp) {
    stats.warningsFilter = [
      // ignore warnings about missing mobile/* modules
      /mobile/,
    ];
    // stats.clientLogLevel = 'error';
  }

  return {
    entry,
    output,
    resolve: {
      extensions: ['.js', '.vue'],
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
      alias: {
        vue$: 'vue/dist/vue.esm.js',
      },
    },
    plugins,
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [
            path.resolve(__dirname, 'src'),
            // vuex-persist needs transpiled for IE 11 to work.
            /vuex-persist/,
            // /\/bootstrap-vue\//,
          ],
          exclude: function (modulePath) {
            return /node_modules/.test(modulePath) && !/vuex-persist/.test(modulePath);
          },
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: './var/babel_cache',
              },
            },
          ],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          oneOf: [
            {
              resourceQuery: /module/,
              use: [
                isProdWeb ? MiniCssExtractPlugin.loader : 'vue-style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                    // Enable CSS Modules
                    modules: {
                      // Customize generated class names
                      localIdentName: '[local]_[hash:base64:8]',
                    },
                  },
                },
                'sass-loader',
              ],
            },
            {
              use: [
                isProdWeb ? MiniCssExtractPlugin.loader : 'vue-style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                  },
                },
                'sass-loader',
              ],
            },
          ],
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {},
        },
        {
          test: /\.mp3$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            useRelativePath: true,
          },
        },
      ],
    },
    optimization: {
      minimizer: [
        new TerserJsPlugin({
          cache: './var/terser_cache',
          parallel: true,
          sourceMap: true,
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
      runtimeChunk: 'single',
    },
    stats,
    devServer: {
      stats,
      contentBase: isMobileApp
        ? [path.join(__dirname, '../mobile/platforms/android/platform_www')]
        : false,
      headers: { 'Access-Control-Allow-Origin': '*' },
      host: '0.0.0.0',
      disableHostCheck: true,
      port: isMobileApp ? 8081 : 8080,
    },
    devtool: isProdWeb || !isMinimalDev ? 'source-map' : false,
  };
};
