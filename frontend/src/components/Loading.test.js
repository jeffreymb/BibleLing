import { shallowMount, createLocalVue } from '@vue/test-utils';
import Loading from './Loading';

const localVue = createLocalVue();
localVue.component('FaIcon', {});

test('hidden when value is false', () => {
  const wrapper = shallowMount(Loading, { localVue });

  wrapper.setProps({
    value: false,
  });

  expect(wrapper.find('.loading').exists()).toBe(false);
});

test('visible when value is true', () => {
  const wrapper = shallowMount(Loading, { localVue });

  wrapper.setProps({
    value: true,
  });

  expect(wrapper.find('.loading').exists()).toBe(true);
});

test('large/center when property passed', () => {
  const wrapper = shallowMount(Loading, { localVue });

  wrapper.setProps({
    value: true,
    size: 'lg',
    center: true,
  });

  expect(wrapper.find('.large').exists()).toBe(true);
  expect(wrapper.find('.center').exists()).toBe(true);
});

test('default properties', () => {
  const wrapper = shallowMount(Loading, { localVue });

  expect(wrapper.find('.loading').exists()).toBe(true);
  expect(wrapper.find('.large').exists()).toBe(false);
  expect(wrapper.find('.center').exists()).toBe(false);
});
