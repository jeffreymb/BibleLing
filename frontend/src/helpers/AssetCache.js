import { Howl } from 'howler';
import _delay from 'lodash/delay';
import { assetPath } from './Media';

let cache = {};

const exists = function (id) {
    return cache[id] !== undefined;
  },
  timeout = function (cacheItem) {
    if (exists(cacheItem.asset.id)) {
      delete cache[cacheItem.asset.id];
    }
  },
  resetTimeout = function (cacheItem) {
    if (cacheItem.timerId) {
      clearTimeout(cacheItem.timerId);
    }
    // delay 15 minutes
    cacheItem.timerId = _delay(timeout, 15 * 60, cacheItem);
  },
  createCacheItem = function (asset, item, promise) {
    const cacheItem = {
      asset,
      item,
      promise,
      timerId: undefined,
    };

    resetTimeout(cacheItem);

    return cacheItem;
  };
export default {
  get: function (asset) {
    if (exists(asset.id)) {
      resetTimeout(cache[asset.id]);
      return cache[asset.id];
    }

    if (asset.type === 'd' && asset.subtype === 'i') {
      const img = new Image(),
        promise = new Promise((resolve, reject) => {
          img.addEventListener('load', () => resolve(cache[asset.id]));
          img.addEventListener('error', reject);
          img.src = assetPath(asset.name);
        });
      cache[asset.id] = createCacheItem(asset, img, promise);
    } else if (asset.type === 'd' && asset.subtype === 'v') {
      // Preloading of videos isn't implemented.
      cache[asset.id] = createCacheItem(asset, undefined, Promise.resolve());
    } else if ((asset.type = 'a')) {
      const howl = new Howl({
          src: [assetPath(asset.name)],
          preload: true,
        }),
        promise = new Promise((resolve, reject) => {
          howl.on('load', () => resolve(cache[asset.id]));
          howl.on('loaderror', reject);
        });
      cache[asset.id] = createCacheItem(asset, howl, promise);
    }

    return cache[asset.id];
  },
  debug() {
    console.log(cache);
  },
};
