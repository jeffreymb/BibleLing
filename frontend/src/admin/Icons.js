import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faExclamationTriangle,
  faArrowsAltV,
  faPlus,
  faPencilAlt,
  faShapes,
} from '@fortawesome/free-solid-svg-icons';

// import {
//     faUnderline,
//     faBold,
//     faItalic,
//     faStrikethrough,
//     faParagraph,
//     faListUl,
//     faListOl,
//     faQuoteRight,
//     faLink,
//     faCode,
//     faUndo,
//     faRedo,
//     faTable
// } from '@fortawesome/free-solid-svg-icons';

library.add(
  faExclamationTriangle,
  faArrowsAltV,
  faPlus,
  faPencilAlt,
  faShapes
  // faUnderline, faBold, faItalic, faStrikethrough, faParagraph,
  // faListUl, faListOl, faQuoteRight, faLink, faCode, faUndo, faRedo,
  // faTable
);
