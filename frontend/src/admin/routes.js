import Index from 'pages/admin/Index';
import Login from 'pages/admin/Login';
import LessonBuilder from 'pages/admin/LessonBuilder';
import Translation from 'pages/admin/Translation';
import Settings from 'pages/admin/Settings';
import AdminUsers from 'pages/admin/UsersAdmin';
import LessonCategories from 'pages/admin/LessonCategories';
import ContentPages from 'pages/admin/ContentPages';
import Contact from 'pages/admin/Contact';
import ManageAssets from 'pages/admin/ManageAssets';
import Licenses from 'pages/admin/Licenses';
import Results from '../pages/admin/Results';
import ResultsUser from '../pages/admin/ResultsUser';
import StudentGroupsIndex from '../pages/admin/StudentGroups/Index';
import StudentGroupsEdit from '../pages/admin/StudentGroups/Edit';

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/lesson',
    name: 'lessons',
    component: LessonBuilder,
  },
  {
    path: '/lesson_categories',
    name: 'lesson_categories',
    component: LessonCategories,
  },
  {
    path: '/content_pages',
    name: 'content_pages',
    component: ContentPages,
  },
  {
    path: '/contact',
    name: 'contact',
    component: Contact,
  },
  {
    path: '/translation',
    name: 'translation',
    component: Translation,
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
  },
  {
    path: '/manage_assets/:assetType',
    name: 'manage_assets',
    component: ManageAssets,
    props: true,
  },
  {
    path: '/licenses',
    name: 'licenses',
    component: Licenses,
  },
  {
    path: '/users_admin',
    name: 'users_admin',
    component: AdminUsers,
  },
  {
    path: '/results',
    name: 'results',
    component: Results,
  },
  {
    path: '/results/:uid',
    name: 'results_user',
    component: ResultsUser,
    props: true,
  },
  {
    path: '/student_groups',
    name: 'student_groups',
    component: StudentGroupsIndex,
  },
  {
    path: '/student_groups/:groupId',
    name: 'student_groups_edit',
    component: StudentGroupsEdit,
    props: true,
  },
];

export default routes;
