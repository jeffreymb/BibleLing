import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import store from 'store/store';
import { USERNAME_ADMIN } from 'store/mutationTypes';

import api from 'helpers/Api';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'hash',
  routes,
});

router.beforeEach((to, from, next) => {
  if (['login'].indexOf(to.name) >= 0) {
    // Igore these routes.
    next();
    return;
  }

  if (store.state.username !== undefined) {
    next();
  } else {
    api
      .get('/auth')
      .then((res) => {
        store.commit(USERNAME_ADMIN, res.data.username);
        next();
      })
      .catch(() => {
        next({ name: 'login' });
      });
  }
});

export default router;
