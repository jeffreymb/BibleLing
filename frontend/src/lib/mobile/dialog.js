import { deviceReady } from './deviceReady';

export async function confirm(message, title, buttonLabels) {
  await deviceReady();

  return new Promise((resolve) => {
    navigator.notification.confirm(
      message,
      (indexClicked) => {
        resolve(indexClicked);
      },
      title,
      buttonLabels
    );
  });
}

export async function alert(message, title, buttonName) {
  await deviceReady();

  return new Promise((resolve) => {
    navigator.notification.alert(
      message,
      () => {
        resolve();
      },
      title,
      buttonName
    );
  });
}
