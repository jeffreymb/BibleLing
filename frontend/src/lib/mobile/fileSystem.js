import { deviceReady } from './deviceReady';

const dirPromise = new Promise(async (resolve, reject) => {
  await deviceReady();
  window.resolveLocalFileSystemURL(
    cordova.file.externalDataDirectory,
    (dir) => {
      resolve(dir);
    },
    (err) => {
      console.error(err);
      reject(err);
    }
  );
});

export const storageName = 'files-external';

export function getDirEntry() {
  return dirPromise;
}

export async function getFile(name, create) {
  const dir = await getDirEntry();

  return new Promise((resolve, reject) => {
    let options = { create };
    if (create) {
      options.exclusive = false;
    }
    dir.getFile(
      name,
      options,
      (fileEntry) => {
        resolve(fileEntry);
      },
      (err) => {
        // console.log(err);
        reject(err);
      }
    );
  });
}

/**
 *
 * @param name String
 * @param blob Blob
 * @return {Promise<FileEntry|false>} False on error.
 */
export async function writeBlob(name, blob) {
  const fileEntry = await getFile(name, true);

  return new Promise((resolve, reject) => {
    fileEntry.createWriter((fileWriter) => {
      fileWriter.onwriteend = () => {
        resolve(fileEntry.fullPath);
      };
      fileWriter.onerror = () => {
        reject(false);
      };
      fileWriter.write(blob);
    });
  });
}

/**
 * Check if a file exists in the file system.
 *
 * @param name
 * @return {Promise<FileEntry|Boolean>}
 */
export async function fileExists(name) {
  try {
    return await getFile(name, false);
  } catch (e) {
    return false;
  }
}

/**
 *
 * @return {Promise<[FileEntry]>}
 */
export async function listFiles() {
  const dir = await getDirEntry();

  const reader = dir.createReader();

  return new Promise((resolve, reject) => {
    reader.readEntries(
      (entries) => {
        resolve(entries);
      },
      (error) => {
        reject(error);
      }
    );
  });
}
