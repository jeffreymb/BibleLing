const ready = new Promise((resolve) => {
  document.addEventListener(
    'deviceready',
    () => {
      resolve(true);
    },
    false
  );
});

export function deviceReady() {
  return ready;
}
