import { Blockquote as Parent } from 'tiptap-extensions';

export default class Blockquote extends Parent {
  get schema() {
    const data = Parent.prototype.schema;

    data.toDOM = () => {
      return ['blockquote', { class: this.options.attrs.class }, 0];
    };
    return data;
  }

  get defaultOptions() {
    const options = Parent.prototype.defaultOptions;

    return {
      attrs: {
        class: 'blockquote',
      },
      ...options,
    };
  }
}
