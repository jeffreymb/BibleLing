import { Paragraph as Parent } from 'tiptap';

export default class Paragraph extends Parent {
  get schema() {
    const schema = Parent.prototype.schema;

    console.log(schema);

    return {
      ...schema,
      attrs: {
        class: {
          default: 'text-left',
        },
      },
      toDOM: (node) => {
        let dom = schema.toDOM(node);
        dom.splice(1, 0, {
          class: node.attrs.class,
        });
        console.log(dom);
        return dom;
      },
      parseDOM: [
        {
          tag: 'p',
          getAttrs: (node) => {
            return {
              textAlign: node.attributes ? node.attributes.class : node.attrs.class,
            };
          },
        },
      ],
    };
  }

  commands(node) {
    const parentCommands = Parent.prototype.commands(node);
    console.log(parentCommands());
    return parentCommands;
  }
}
