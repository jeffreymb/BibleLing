import { Table as Parent } from 'tiptap-extensions';

export default class Table extends Parent {
  get schema() {
    console.log(this);
    const schema = Parent.prototype.schema;

    return {
      ...schema,
      toDOM: (node) => {
        const orgDom = schema.toDOM(node);
        orgDom.splice(1, 0, { class: 'table table-striped' });
        return orgDom;
      },
    };
  }
}
