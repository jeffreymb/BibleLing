import NoSleep from 'nosleep.js';

// It needs to be set on the window to work.
window.nosleep = new NoSleep();

export const noSleep = window.nosleep;
