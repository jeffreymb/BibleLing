import Vue from 'vue';

const createRange = function createRange(node, chars, range) {
  if (!range) {
    range = document.createRange();
    range.selectNode(node);
    range.setStart(node, 0);
  }

  if (chars.count === 0) {
    range.setEnd(node, chars.count);
  } else if (node && chars.count > 0) {
    if (node.nodeType === Node.TEXT_NODE) {
      if (node.textContent.length < chars.count) {
        chars.count -= node.textContent.length;
      } else {
        range.setEnd(node, chars.count);
        chars.count = 0;
      }
    } else {
      for (let lp = 0; lp < node.childNodes.length; lp++) {
        range = createRange(node.childNodes[lp], chars, range);

        if (chars.count === 0) {
          break;
        }
      }
    }
  }

  return range;
};

const isChildOf = function (node, parentId) {
  while (node !== null) {
    if (node.id === parentId) {
      return true;
    }
    node = node.parentNode;
  }
  return false;
};

export const getCurrentCaretPosition = function (id) {
  let selection = window.getSelection(),
    charCount = -1,
    node;

  if (selection.focusNode && isChildOf(selection.focusNode, id)) {
    node = selection.focusNode;
    charCount = selection.focusOffset;

    while (node) {
      if (node.id === id) {
        break;
      }

      if (node.previousSibling) {
        node = node.previousSibling;
        charCount += node.textContent.length;
      } else {
        node = node.parentNode;
        if (node === null) {
          break;
        }
      }
    }
  }

  return charCount;
};

export default Vue.directive('html-caret', {
  bind(el, binding) {
    // console.log('bind', `"${binding.value}"`);
    el.innerHTML = binding.value;
  },
  update(el, binding) {
    let currentCaretPos = getCurrentCaretPosition(el.id);
    // console.log(currentCaretPos, el.id, `"${binding.value}"`);
    el.innerHTML = binding.value;

    if (currentCaretPos > 0) {
      let textLength = el.textContent.length;
      if (currentCaretPos > textLength) {
        console.warn('oops', currentCaretPos, textLength);
        currentCaretPos = textLength;
      }
      // console.log(currentCaretPos, binding.value.length, el.textContent.length);
      let range = createRange(el, { count: currentCaretPos }),
        sel = window.getSelection();
      range.collapse(false);
      sel.removeAllRanges();
      sel.addRange(range);
    }
  },
});
