import Base from './BaseData';

export async function load(opt) {
  return Base(`translation/${opt.id}`, opt.force);
}
