import Base from './BaseData';

export async function load(lang, opt) {
  return Base(`config/${lang}`, opt.force);
}
