import Base from './BaseData';

export async function load(opt) {
  return Base('lesson', opt.force);
}
