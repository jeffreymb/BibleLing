import api from 'helpers/Api';

let promise = undefined;

export function load() {
  if (promise) {
    return promise;
  }

  promise = new Promise((resolve, reject) => {
    api
      .get('sites')
      .then((resp) => {
        resolve(resp.data);
        promise = undefined;
        return resp.data;
      })
      .catch((err) => {
        reject(err);
        promise = undefined;
      });
  });

  return promise;
}
