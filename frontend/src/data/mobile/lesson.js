import { query } from './db';

export async function offlineSummary() {
  const stats = await query(
    'SELECT COUNT(id) AS qty, lesson_id FROM activities GROUP BY lesson_id'
  );

  let data = {};
  for (let i = 0; i < stats.length; i++) {
    const d = stats[i];

    data[d.lesson_id] = d.qty;
  }

  return data;
}

export async function removeLesson(lessonId) {
  await query('DELETE FROM activities WHERE lesson_id = ?', [lessonId]);
  return true;
}
