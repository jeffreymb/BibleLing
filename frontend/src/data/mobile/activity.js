import { query } from './db';
import { generateUrl } from '../Activity';
import api from 'helpers/Api';

import { downloadAsssets, loadFromDb as loadAssetFromDb } from './assets';

export async function load(activityId) {
  const res = await query(
    'SELECT id, lesson_id, type, config, pages FROM activities where id = ?',
    [activityId]
  );

  if (res.length === 0) {
    return undefined;
  }
  const activity = {
    id: res[0].id,
    lesson_id: res[0].lesson_id,
    type: res[0].type,
    config: JSON.parse(res[0].config),
    pages: JSON.parse(res[0].pages),
  };

  await processActivityAssets(activity, PROCESS_MODE_LOAD);

  return activity;
}

export async function isOffline(activityId) {
  const res = await query('SELECT id FROM activities WHERE id = ?', [activityId]);

  return res.length > 0;
}

/**
 *
 * @param activityId
 * @return {Promise<Boolean>}
 */
export async function downloadActivity(activityId) {
  const res = await api.get(generateUrl(activityId));

  const activity = await downloadAsssets(res.data);

  await processActivityAssets(activity, PROCESS_MODE_SAVE);

  await query(
    'REPLACE INTO activities (id, lesson_id, type, config, pages) values (?, ?, ?, ?, ?)',
    [
      activity.id,
      activity.lesson_id,
      activity.type,
      JSON.stringify(activity.config),
      JSON.stringify(activity.pages),
    ]
  );

  return await load(activityId);
}

const PROCESS_MODE_SAVE = 'save';
const PROCESS_MODE_LOAD = 'load';

/**
 *
 * @param activity
 * @param mode
 * @return {Promise<void>} Alters the passed in activity.
 */
async function processActivityAssets(activity, mode) {
  for (let pageId = 0; pageId < activity.pages.length; pageId++) {
    const page = activity.pages[pageId];

    if (!page.items) {
      continue;
    }

    for (let itemId = 0; itemId < page.items.length; itemId++) {
      const item = page.items[itemId];

      if (!item.assets) {
        continue;
      }

      for (let assetId = 0; assetId < item.assets.length; assetId++) {
        const asset = item.assets[assetId];
        if (mode === PROCESS_MODE_SAVE) {
          // We only store the asset's name with the activity.
          item.assets[assetId] = asset.name;
        } else if (mode === PROCESS_MODE_LOAD) {
          // Load the asset from the db.
          item.assets[assetId] = await loadAssetFromDb(asset);
        }
      }
    }
  }
}
