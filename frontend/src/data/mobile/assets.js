import db from './db';
import axios from 'axios';

import { fileExists, writeBlob, listFiles } from '../../lib/mobile/fileSystem';
import { screenWidth } from '../../lib/mobile/device';
import { videoScaleFactor } from '../../helpers/Media';

import _isObject from 'lodash/isObject';
import _isArray from 'lodash/isArray';
import _keys from 'lodash/keys';
import _concat from 'lodash/concat';
import _sort from 'lodash/sortBy';
import _reverse from 'lodash/reverse';

/**
 *
 * @param asset
 * @return {Promise<void>}
 */
async function saveToDb(asset) {
  await db.query(
    'REPLACE INTO assets (name, type, subtype, caption, credits_source, credits_person, license, metadata) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
    [
      asset.name,
      asset.type,
      asset.subtype,
      asset.caption,
      asset.credits_source,
      asset.credits_person,
      asset.license,
      JSON.stringify(asset.metadata),
    ]
  );
}

export async function loadFromDb(name) {
  const results = await db.query('SELECT * FROM assets WHERE name = ? LIMIT 1', [name]);

  if (results.length === 0) {
    return undefined;
  }
  const asset = results[0];

  asset.metadata = JSON.parse(asset.metadata);

  return asset;
}

export async function listDisplay() {
  const results = await db.query("SELECT * FROM assets WHERE type = 'd' ORDER BY name", []);

  for (let i = 0; i < results.length; i++) {
    results[i].metadata = JSON.parse(results[i].metadata);
  }

  return results;
}

export async function clearDatabase() {
  // Clear the internal cache.
  inProgress = [];
  await db.query('DELETE FROM assets');

  return true;
}

function parseFileId(name) {
  return name.split('.')[0].split('_')[0];
}

export async function deleteUnusedFiles() {
  const usedFileNames = await db.query('SELECT name FROM assets ORDER BY name');

  for (let i = 0; i < usedFileNames.length; i++) {
    usedFileNames[i] = parseFileId(usedFileNames[i].name);
  }

  const files = await listFiles();

  let processes = [];

  for (let fileId = 0; fileId < files.length; fileId++) {
    const f = files[fileId];

    if (!usedFileNames.includes(parseFileId(f.name))) {
      const p = new Promise((resolve, reject) => {
        f.remove(
          () => {
            resolve(true);
          },
          (error) => {
            reject(error);
          }
        );
      });

      processes.push(p);
    }
  }

  await Promise.all(processes);

  return true;
}

let qtyActivitiesInProgress = 0,
  inProgress = [];

/**
 * Downloads all assets found in an item and returns the updated item.
 * (Not all video sizes are downloaded, so the ones that aren't downloaded must be removed.
 *
 * @param item
 * @return {Promise<Object>}
 */
export async function downloadAsssets(item) {
  qtyActivitiesInProgress++;
  const assets = findAssets(item);

  // console.log(JSON.parse(JSON.stringify(assets)));

  let actions = [];

  const minSize = screenWidth * videoScaleFactor;

  for (let i = 0; i < assets.length; i++) {
    const a = assets[i];

    if (inProgress.indexOf(a.id) >= 0) {
      // This asset is already being downloaded.
      continue;
    }

    if (
      // Images
      (a.type === 'd' && a.subtype === 'i') ||
      // Audio
      a.type === 'a'
    ) {
      // Image or Audio
      inProgress.push(a.id);
      actions.push(downloadFile(a.name));
    } else if (a.type === 'd' && a.subtype === 'v') {
      // Video

      inProgress.push(a.id);
      // Download the poster image.
      actions.push(downloadFile(a.metadata.poster));

      let sizes = [];

      if (a.metadata.sizes) {
        sizes = _reverse(_sort(a.metadata.sizes, [(s) => s.width]));
      }

      let videoName = '';

      // We iterate through the array backwards so that we can safely remove
      // the items we don't download.
      for (let sizeId = sizes.length - 1; sizeId > 0; sizeId--) {
        const s = sizes[sizeId];

        if (!videoName && s.width >= minSize) {
          videoName = s.name;
        } else {
          // Remove the ones we don't download.
          sizes.splice(sizeId, 1);
        }
      }

      if (!videoName && sizes.length > 0) {
        // Grab the largest one.
        videoName = sizes[0].name;
        // remove all but the first one.
        sizes.splice(1, sizes.length);
      }

      a.metadata.sizes = sizes;
      if (videoName) {
        actions.push(downloadFile(videoName));
      }
    } else {
      console.error(['Unrecognized asset', a]);
      // unrecognized file.
    }

    actions.push(saveToDb(a));
  }

  await Promise.all(actions);

  qtyActivitiesInProgress--;

  if (qtyActivitiesInProgress === 0) {
    inProgress = [];
    console.info('RESETTING inProgress');
  }
  return item;
}

export function findAssets(item) {
  let results = [];
  if (isAsset(item)) {
    results.push(item);
  } else if (_isArray(item)) {
    for (let i = 0; i < item.length; i++) {
      const assets = findAssets(item[i]);

      if (assets.length > 0) {
        results = _concat(results, findAssets(item[i]));
      }
    }
  } else if (_isObject(item)) {
    const keys = _keys(item);

    for (let i = 0; i < keys.length; i++) {
      const k = keys[i],
        assets = findAssets(item[k]);
      if (assets.length > 0) {
        results = _concat(results, assets);
      }
    }
  }
  return results;
}

export function isAsset(item) {
  return _isObject(item) && item.type !== undefined && item.subtype !== undefined;
}

/**
 *
 * @param name String
 * @return {Promise<Boolean>}
 */
export async function downloadFile(name) {
  const exists = await fileExists(name);

  if (exists !== false) {
    return true;
  }
  const url = `${MOBILE_DOMAIN}/assets/${name}`;

  const fileResponse = await axios({
    url: url,
    method: 'GET',
    responseType: 'blob',
  });

  const fileEntry = await writeBlob(name, fileResponse.data);

  return fileEntry !== false;
}
