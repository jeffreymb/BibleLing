import migrations from './migrations';
import { getCurrentVersion, setCurrentVersion } from './migrations';

let db,
  isInitialized = false,
  dbPromise;

async function getDb() {
  if (dbPromise) {
    return dbPromise;
  }
  dbPromise = new Promise((resolve, reject) => {
    if (isInitialized) {
      resolve(db);
      return;
    }
    document.addEventListener('deviceready', () => {
      db = window.sqlitePlugin.openDatabase(
        {
          name: 'langapp.db',
          location: 'default',
        },
        async () => {
          const currentVersion = await getCurrentVersion(db);

          if (migrations.length > currentVersion) {
            for (let i = currentVersion; i < migrations.length; i++) {
              // Execute migration.
              db.transaction(async (tnx) => {
                console.log(`Executing migration ${i + 1}`);
                migrations[i](tnx);
              });
            }

            await setCurrentVersion(db, migrations.length);
          }

          isInitialized = true;
          resolve(db);
        },
        (err) => {
          console.error('Open database ERROR: ' + JSON.stringify(err));
          isInitialized = false;
          reject(err);
        }
      );
    });
  });

  return dbPromise;
}

export async function query(sql, params) {
  const db = await getDb();

  return new Promise((resolve, reject) => {
    db.executeSql(
      sql,
      params,
      (res) => {
        let data = [];
        for (let i = 0; i < res.rows.length; i++) {
          data.push(res.rows.item(i));
        }
        resolve(data);
      },
      (err) => {
        console.error(err);
        reject(err);
      }
    );
  });
}

export async function getVal(key) {
  const res = await query('SELECT v FROM kv WHERE k = ? LIMIT 1', [key]);

  if (res.length === 0) {
    return undefined;
  }

  return JSON.parse(res[0].v);
}

export async function setVal(key, val) {
  await query('REPLACE INTO kv (k, v) VALUES (?, ?)', [key, JSON.stringify(val)]);

  return await getVal(key);
}

export async function hasVal(key) {
  const val = await getVal(key);

  return val !== undefined;
}

export default {
  query,
  getVal,
  setVal,
  hasVal,
};
