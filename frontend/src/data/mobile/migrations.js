export default [
  (tnx) => {
    tnx.executeSql('CREATE TABLE kv (k TEXT NOT NULL PRIMARY KEY, v TEXT DEFAULT NULL)');
    tnx.executeSql(
      'CREATE TABLE assets (name TEXT NOT NULL PRIMARY KEY, type TEXT, subtype TEXT, caption TEXT, credits_source TEXT, credits_person TEXT, license TEXT, metadata TEXT)'
    );
    tnx.executeSql(
      'CREATE TABLE activities (id INTEGER NOT NULL PRIMARY KEY, lesson_id INTEGER NOT NULL, type TEXT NOT NULL, config TEXT, pages TEXT)'
    );
  },
];

export function getCurrentVersion(db) {
  return new Promise((resolve, reject) => {
    db.executeSql(
      'PRAGMA user_version',
      [],
      (res) => {
        const version = res.rows.item(0).user_version;
        console.log(`Current database version is ${version}`);

        resolve(version);
      },
      (err) => reject(err)
    );
  });
}

export function setCurrentVersion(db, version) {
  return new Promise((resolve, reject) => {
    db.executeSql(
      'PRAGMA user_version = ' + version,
      [],
      () => {
        console.log(`Database version set to ${version}`);
        resolve(true);
      },
      (err) => reject(err)
    );
  });
}
