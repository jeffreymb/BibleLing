import Base from './BaseData';

export function load(id, lang, force) {
  return Base(`content_page/${id}/${lang}`, force);
}
