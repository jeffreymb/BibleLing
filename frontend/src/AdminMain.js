import './MainShared';
import './admin/Icons';

import Vue from 'vue';

import { BButtonGroup, BPopover } from 'bootstrap-vue';
Vue.component('BButtonGroup', BButtonGroup);
Vue.component('BPopover', BPopover);

import router from 'admin/Router';

import ga from 'config/ga';
ga(router);

import store from 'store/store';
import AdminModule from 'store/admin/AdminStore';

store.registerModule('admin', AdminModule);

import { LOCALE } from './store/mutationTypes';
// The admin UI is always english.
store.commit(LOCALE, 'en');

import i18n from 'i18n';
import App from 'App';

import api from 'helpers/Api';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, api);

import { init } from 'config/main';

init(false).then();

new Vue({
  router,
  store,
  i18n,
  el: '#app',
  render: (createElement) =>
    createElement(App, {
      props: {
        enableSwipeMenu: false,
      },
    }),
});
