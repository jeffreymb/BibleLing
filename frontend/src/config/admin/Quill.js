import Quill from 'quill';
// import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';

const Parchment = Quill.import('parchment');

const AlignClass = new Parchment.Attributor.Class('align', 'text', {
  scope: Parchment.Scope.BLOCK,
  whitelist: ['right', 'center', 'justify'],
});

// Attributors
Quill.register({
  'attributors/class/align': AlignClass,
});

// Formats
Quill.register({
  'formats/align': AlignClass,
});

export default Quill;
