import 'MainShared';
import 'mobile/Icons';
import 'styles/mobile.scss';

import Vue from 'vue';

import store from 'store/store';
import mobileStore from 'store/mobileStore';
import { ONLINE, APP_INITIALIZED } from 'store/mobileStore';

store.registerModule('mobile', mobileStore);

import i18n from 'i18n';
import router from 'client/Router';

document.addEventListener('deviceready', onDeviceReady, false);

import db from 'data/mobile/db';
import { init } from 'config/main';

import App from 'App';

function onDeviceReady() {
  if (navigator.connection.type !== Connection.NONE) {
    store.commit(ONLINE, true);
  } else {
    store.commit(ONLINE, false);
  }

  db.getVal(`config/${store.state.locale}`)
    .then((settings) => {
      if (settings === undefined) {
        // Forward to the offline mgt page.
        router.replace({ name: 'offline_mgt' }, () => {
          init().then(() => {
            store.commit(APP_INITIALIZED, true);
          });
        });
      } else {
        init().then(() => {
          store.commit(APP_INITIALIZED, true);
        });
      }
    })
    .catch((err) => alert(err));

  new Vue({
    router,
    store,
    i18n,
    el: '#app',
    render: (createElement) => createElement(App),
  });
}

document.addEventListener(
  'online',
  function () {
    store.commit(ONLINE, true);
  },
  false
);

document.addEventListener(
  'offline',
  function () {
    store.commit(ONLINE, false);
  },
  false
);
