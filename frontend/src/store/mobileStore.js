export const ONLINE = 'ONLINE';
export const APP_INITIALIZED = 'APP_INITIALIZED';

export default {
  state: {
    online: false,
    appInitialized: false,
  },
  mutations: {
    [ONLINE](state, payload) {
      state.online = payload;
    },
    [APP_INITIALIZED](state, payload) {
      state.appInitialized = payload;
    },
  },
};
