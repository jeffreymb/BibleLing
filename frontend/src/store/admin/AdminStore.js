import { buildLoader } from '../ApplicationData';
import { load as SitesLoader } from '../../data/admin/Sites';

const SITES = 'SITES';
const SITES_LOADING = 'SITES_LOADING';

export default {
  state: {
    sites: undefined,
    sitesLoading: true,
  },
  mutations: {
    [SITES](state, payload) {
      state.sites = payload;
    },
    [SITES_LOADING](state, payload) {
      state.sitesLoading = payload;
    },
  },
  actions: {
    loadSites: buildLoader(SitesLoader, SITES, SITES_LOADING),
  },
};
