#!/usr/bin/env node
import dotenv from 'dotenv';
dotenv.config();

import { appDebug as debugFactory } from '../config/debug';

const debug = debugFactory.extend('boot');

debug('initializing');

import app from '../app';

debug('app init complete');

import http from 'http';

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

debug('server created');

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  debug('ERROR: ' + error);
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

server.on('error', onError);
server.on('listening', onListening);

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

debug(`ready to listen on port ${port}`);
/**
 * Listen on provided port, on all network interfaces.
 */
try {
  server.listen(port);
} catch (error) {
  debug('error listening', error);
}
