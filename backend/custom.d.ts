import { UserInterface } from './db/user';
import { auth } from 'firebase-admin';

declare global {
  namespace Express {
    interface Request {
      siteId: string;
      adminUser?: UserInterface;
      fbUser?: auth.DecodedIdToken;
    }
  }
}
