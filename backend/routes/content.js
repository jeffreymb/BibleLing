import express from 'express';
import { searchContent, getContent, saveContent } from '../db/content';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();

router.get('/search', isAuthenticated, (req, res, next) => {
    searchContent(req.siteId, req.query.q, req.query.l)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.get('/:id(\\d+)', isAuthenticated, (req, res, next) => {
    getContent(req.params.id)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
    saveContent(req.siteId, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

export default router;
