import express from 'express';
import {
    deleteRecipient,
    getContact,
    getContactPage,
    getRecipient,
    saveContact,
    saveOrder,
    saveRecipient,
} from '../db/contact';
import { listAll } from '../db/content_page';
import isAuthenticated from '../lib/isAuthenticated';
import svgCaptcha from 'svg-captcha';
import { sendMessage } from '../lib/mailer';
const router = express.Router();

router.get('/captcha.svg', (req, res) => {
    const captcha = svgCaptcha.create({
        noise: 4,
        size: 5,
        color: true,
    });

    req.session.captcha = captcha.text;

    res.type('svg');
    res.status(200).send(captcha.data);
});

router.get('/page', (req, res, next) => {
    getContactPage(req.siteId)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('/submit', async (req, res, next) => {
    const submission = req.body;

    if (submission.captcha !== req.session.captcha) {
        res.json({
            captcha: false,
        });
        return;
    }

    const recipient = await getRecipient(req.siteId, submission.reason);

    await sendMessage({
        to: recipient.recipient,
        from: submission.email,
        subject: `[${req.siteId} contact] ${submission.subject}`,
        text: submission.message,
    });
    res.json(true);
});

router.get('', isAuthenticated, (req, res, next) => {
    const queries = [];
    let contact, contentPages;

    queries.push(
        getContact(req.siteId)
            .then((data) => {
                contact = data;
            })
            .catch((err) => next(err))
    );

    queries.push(
        listAll(req.siteId)
            .then((data) => {
                contentPages = data;
            })
            .catch((err) => next(err))
    );

    Promise.all(queries)
        .then(() => {
            res.json({
                pages: contentPages,
                contact: contact,
            });
        })
        .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
    saveContact(req.siteId, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('/recipient/new', isAuthenticated, (req, res, next) => {
    saveRecipient(req.siteId, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.delete('/recipient/:id(\\d+)', isAuthenticated, (req, res, next) => {
    deleteRecipient(req.siteId, req.params.id)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('/recipient/order', isAuthenticated, (req, res, next) => {
    saveOrder(req.siteId, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

export default router;
