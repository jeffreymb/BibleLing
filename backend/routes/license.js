import { deleteLicense, listAllLicenses, saveLicense } from '../db/license';
import isAuthenticated from '../lib/isAuthenticated';
import { Router } from 'express';
const router = Router();

router.get('', isAuthenticated, (req, res, next) => {
  listAllLicenses()
    .then((licenses) => res.json(licenses))
    .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
  listAllLicenses()
    .then((current) => {
      const queries = [],
        saved = [];
      const updated = req.body;

      for (let i = 0; i < updated.length; i++) {
        if (!updated[i].id) {
          // Skip blank lines
          continue;
        }

        queries.push(saveLicense(updated[i]));
        saved.push(updated[i].id);
      }

      for (let i = 0; i < current.length; i++) {
        if (!saved.includes(current[i].id)) {
          queries.push(deleteLicense(current[i].id));
        }
      }

      Promise.all(queries)
        .then(() => {
          listAllLicenses()
            .then((licenses) => res.json(licenses))
            .catch((err) => next(err));
        })
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

export default router;
