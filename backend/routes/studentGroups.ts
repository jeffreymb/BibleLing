import { isAdmin } from '../lib/isAuthenticated';
import { getFireBaseApp } from '../config/firebaseAdmin';
import { Router, Request, Response, NextFunction } from 'express';
import { NOT_FOUND, INTERNAL_SERVER_ERROR } from 'http-status-codes';
import {
  addStudentsToGroup,
  createGroup,
  deleteGroup,
  listGroups,
  listStudents,
  loadGroup,
  removeStudentFromGroup,
  updateGroup,
} from '../db/studentGroups';
import admin from 'firebase-admin';

const router = Router();

router.get(
  '',
  isAdmin,
  async (req: Request<unknown, unknown, unknown, { includeInactive: string }>, res: Response) => {
    const includeInactive = req.query.includeInactive
      ? req.query.includeInactive in ['1', 'true']
      : false;
    const groups = await listGroups(req.siteId, includeInactive);

    res.json(groups);
  }
);

router.get('/:groupId(\\d+)', isAdmin, async (req: Request, res: Response) => {
  const group = await loadGroup(req.siteId, parseFloat(req.params.groupId));

  if (!group) {
    res.sendStatus(NOT_FOUND);
    return;
  }

  res.json({
    group,
  });
});

router.post('/new', isAdmin, async (req: Request, res: Response, next: NextFunction) => {
  const name = req.body.name,
    inactive = req.body.inactive;

  try {
    const group = await createGroup(req.siteId, { name, inactive });

    res.json({ group });
  } catch (e) {
    next(e);
  }
});

router.post('/:id(\\d+)', isAdmin, async (req: Request, res: Response, next: NextFunction) => {
  const name = req.body.name,
    inactive = req.body.inactive;

  try {
    const group = await updateGroup(req.siteId, {
      name,
      inactive,
      id: parseFloat(req.params.id),
    });

    res.json({ group });
  } catch (e) {
    next(e);
  }
});

router.delete('/:id(\\d+)', isAdmin, async (req: Request, res: Response, next: NextFunction) => {
  try {
    await deleteGroup(req.siteId, parseFloat(req.params.id));
    res.json(null);
  } catch (e) {
    next(e);
  }
});

router.get(
  '/:id(\\d+)/students',
  isAdmin,
  async (req: Request, res: Response, next: NextFunction) => {
    const groupId = parseFloat(req.params.id);

    try {
      const [userIds, app] = await Promise.all([
        listStudents(req.siteId, groupId),
        getFireBaseApp(req.siteId),
      ]);

      if (!app) {
        res.sendStatus(INTERNAL_SERVER_ERROR);
        return;
      }

      if (!userIds) {
        res.json([]);
        return;
      }

      const users = await app.auth().getUsers(userIds.map((u) => ({ uid: u.user_id })));

      res.json(
        users.users.map((u) => ({
          uid: u.uid,
          name: u.displayName,
          email: u.email,
          photoUrl: u.photoURL,
        }))
      );
    } catch (e) {
      next(e);
    }
  }
);

interface UserSummary {
  uid: string;
  name?: string;
  email?: string;
  photoUrl?: string;
}

router.get(
  '/:id(\\d+)/students/find',
  isAdmin,
  async (req: Request, res: Response, next: NextFunction) => {
    const groupId = parseFloat(req.params.id),
      [app, groupUsers] = await Promise.all([
        getFireBaseApp(req.siteId),
        listStudents(req.siteId, groupId),
      ]),
      groupUserIds = groupUsers.map((u) => u.user_id);

    if (!app) {
      res.sendStatus(INTERNAL_SERVER_ERROR);
      return;
    }

    const criteria: Partial<Record<keyof admin.auth.UserRecord, string>> = {};

    if (req.query.email) {
      criteria.email = (req.query.email as string).toLowerCase();
    }
    if (req.query.name) {
      criteria.displayName = (req.query.name as string).toLowerCase();
    }

    const filter = (user: admin.auth.UserRecord): boolean => {
      for (const k in criteria) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (!user[k].toLowerCase().includes(criteria[k])) {
          return false;
        }
      }
      return !groupUserIds.includes(user.uid);
    };
    const map = (user: admin.auth.UserRecord): UserSummary => ({
      uid: user.uid,
      name: user.displayName,
      email: user.email,
      photoUrl: user.photoURL,
    });

    try {
      const usersResult = await app.auth().listUsers(100);

      const users: Array<{ uid: string }> = usersResult.users.filter(filter).map(map);
      res.json(users);
    } catch (e) {
      next(e);
    }
  }
);

router.post(
  '/:id(\\d+)/students',
  isAdmin,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      await addStudentsToGroup(req.siteId, parseFloat(req.params.id), req.body);
    } catch (e) {
      next(e);
      return;
    }
    res.json(true);
  }
);

router.delete(
  '/:groupId(\\d+)/students/:studentId',
  isAdmin,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const groupId = parseFloat(req.params.groupId),
        userId = String(req.params.studentId);

      await removeStudentFromGroup(req.siteId, groupId, userId);
    } catch (e) {
      next(e);
      return;
    }
    res.json(null);
  }
);

export default router;
