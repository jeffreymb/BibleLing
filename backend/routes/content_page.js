import express from 'express';
import {
    create,
    deletePage,
    getForLang,
    listAll,
    saveContent,
    saveOrder,
    savePage,
    STATE_PUBLSISHED,
    STATE_DRAFT,
} from '../db/content_page';
import { deleteTranslation } from '../db/translation';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();

router.get('', isAuthenticated, (req, res, next) => {
    listAll(req.siteId)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('/order/save', isAuthenticated, (req, res, next) => {
    saveOrder(req.siteId, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.post('/new/:id', isAuthenticated, (req, res, next) => {
    create(req.siteId, req.params.id)
        .then((data) => res.json(data))
        .catch((err) => {
            if (err.code === 'ER_DUP_ENTRY') {
                res.status(409).json(false);
            } else {
                next(err);
            }
        });
});

router.post('/save/:id', isAuthenticated, (req, res, next) => {
    savePage(req.siteId, req.params.id, req.body)
        .then((data) => res.json(data))
        .catch((err) => next(err));
});

router.delete('/:id', isAuthenticated, (req, res, next) => {
    deletePage(req.siteId, req.params.id)
        .then((data) => {
            res.json(data);
            deleteTranslation(req.siteId, '_content_page_title_' + req.params.id);
        })
        .catch((err) => next(err));
});

router.get('/:id/:lang', (req, res, next) => {
    const state = req.adminUser && req.adminUser.id ? STATE_DRAFT : STATE_PUBLSISHED;
    getForLang(req.siteId, req.params.id, req.params.lang, state)
        .then((page) => {
            if (!page) {
                res.status(404).json(false);
            } else {
                res.json(page);
            }
        })
        .catch((err) => next(err));
});

router.post('/:id/:lang', isAuthenticated, (req, res, next) => {
    saveContent(req.siteId, req.params.id, req.params.lang, req.body)
        .then((result) => res.json(result))
        .catch((err) => next(err));
});

export default router;
