import { Router } from 'express';
import { deleteLesson, getLessons, saveLesson, saveOrder } from '../db/lesson';
import { isAdmin } from '../lib/isAuthenticated';
const router = Router();

router.get('', async (req, res) => {
  const showDraft = !!(req.adminUser && req.adminUser.id);
  const rows = await getLessons(req.siteId, showDraft);
  res.json(rows);
});

router.post('', isAdmin, (req, res, next) => {
  saveLesson(req.siteId, {})
    .then((id) => res.json(id))
    .catch((err) => next(err));
});

router.post('/:id(\\d+)', isAdmin, (req, res, next) => {
  const lesson = req.body;
  lesson.id = req.params.id;

  saveLesson(req.siteId, lesson)
    .then(() => {
      const showDraft = !!(req.adminUser && req.adminUser.id);
      getLessons(req.siteId, showDraft)
        .then((lessons) => res.json(lessons))
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

router.post('/order', isAdmin, async (req, res, next) => {
  const lessons = req.body;
  for (let i = 0; i < lessons.length; i++) {
    lessons[i].order = i;
  }

  try {
    const qty = await saveOrder(req.siteId, lessons);
    res.json({ updated: qty });
  } catch (e) {
    next(e);
  }
});

router.delete('/:id(\\d+)', isAdmin, async (req, res, next) => {
  try {
    const result = await deleteLesson(req.siteId, parseFloat(req.params.id));
    res.json(result);
  } catch (e) {
    next(e);
  }
});

export default router;
