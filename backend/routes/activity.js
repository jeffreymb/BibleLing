import express from 'express';
import { deleteActivity, getActivity, saveOrder, updateActivity } from '../db/activity';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();

router.get('/:id(\\d+)', (req, res, next) => {
    getActivity(req.params.id)
        .then((activity) => {
            if (!activity) {
                res.status(404).send();
            } else {
                res.json(activity);
            }
        })
        .catch((err) => next(err));
});

router.post('/:id(\\d+)', isAuthenticated, (req, res, next) => {
    updateActivity(req.body)
        .then((activity) => res.json(activity))
        .catch((err) => next(err));
});

router.delete('/:id(\\d+)', isAuthenticated, (req, res, next) => {
    getActivity(req.params.id)
        .then((activity) => {
            if (activity) {
                deleteActivity(activity, req.siteId)
                    .then(() => {
                        res.json(true);
                    })
                    .catch((err) => next(err));
            }
        })
        .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
    updateActivity(req.body)
        .then((activity) => res.json(activity))
        .catch((err) => next(err));
});

router.post('/order', isAuthenticated, (req, res, next) => {
    const activities = req.body;
    for (let i = 0; i < activities.length; i++) {
        activities[i].order = i;
    }

    saveOrder(activities)
        .then((qty) => res.json({ updated: qty }))
        .catch((err) => next(err));
});

export default router;
