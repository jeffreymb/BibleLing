import express from 'express';
import {
  deleteLessonCategory,
  listAll,
  saveLessonCategory,
  saveOrder,
} from '../db/lesson_category';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();

router.get('', isAuthenticated, (req, res, next) => {
  listAll(req.siteId)
    .then((rows) => res.json(rows))
    .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
  saveLessonCategory(req.siteId, {})
    .then((id) => res.json(id))
    .catch((err) => next(err));
});

router.post('/order', isAuthenticated, (req, res, next) => {
  saveOrder(req.siteId, req.body)
    .then((qty) => res.json(qty))
    .catch((err) => next(err));
});

router.post('/:id(\\d+)', isAuthenticated, (req, res, next) => {
  saveLessonCategory(req.siteId, req.body)
    .then((result) => res.json(result))
    .catch((err) => next(err));
});

router.delete('/:id(\\d+)', isAuthenticated, (req, res, next) => {
  deleteLessonCategory(req.siteId, req.params.id)
    .then((deleted) => {
      res.json(deleted);
    })
    .catch((err) => next(err));
});

export default router;
