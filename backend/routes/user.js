import isAuthenticated from '../lib/isAuthenticated';
import { deleteUser, listAll, saveUser } from '../db/user';
import bcrypt from 'bcryptjs';
import { Router } from 'express';

const router = Router();

router.get('', isAuthenticated, async (req, res, next) => {
  listAll(req.siteId)
    .then((users) => {
      res.json(users);
    })
    .catch((err) => next(err));
});

router.post('', isAuthenticated, (req, res, next) => {
  const user = req.body;

  if (user.password) {
    bcrypt.hash(user.password, 10, (err, hash) => {
      if (err) {
        next(err);
        return;
      }
      user.password = hash;
      saveUser(req.siteId, user)
        .then((updatedUser) => res.json(updatedUser))
        .catch((err) => next(err));
    });
  }
});

router.delete('/:id(\\d+)', isAuthenticated, (req, res, next) => {
  deleteUser(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
});

export default router;
