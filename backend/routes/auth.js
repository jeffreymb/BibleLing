import express from 'express';
import passport from '../config/passport';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();
router.get('/login', function (req, res) {
  res.send(
    '<form action="/api/auth/login" method="post">' +
      '<input type="text" name="username">' +
      '<input type="password" name="password">' +
      '<input type="submit"></form>'
  );
});

router.post('/login', passport.authenticate('local'), function (req, res) {
  res.json({ id: req.adminUser.id, username: req.adminUser.email });
});

router.get('/logout', (req, res) => {
  req.logout();
  res.send('you have been logged out');
});

router.get('', isAuthenticated, (req, res) => {
  res.status(200).json({ id: req.adminUser.id, username: req.adminUser.email });
});

export default router;
