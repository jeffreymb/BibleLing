import { getLanguages, saveLanguage } from '../db/translation';
import { Router } from 'express';
const router = Router();

router.get('', (req, res, next) => {
    getLanguages(req.siteId, req.params.lang)
        .then((rows) => res.json(rows))
        .catch((err) => next(err));
});

router.post('', (req, res, next) => {
    if (!req.body.lang) {
        res.status(400).send('Missing "lang" value');
        return;
    }
    saveLanguage(req.siteId, req.body.lang)
        .then(() => res.send())
        .catch((err) => next(err));
});

export default router;
