import express from 'express';
import { isAdmin } from '../lib/isAuthenticated';
import {
  credits,
  deleteAsset,
  getAsset,
  listItems,
  saveAsset,
  saveSites,
  search,
} from '../db/asset';
import { importImage, searchImages } from '../lib/mediawiki';
import { deleteFiles, generateFilename, processImage, processVideo } from '../lib/asset';
import uploadDest from '../config/uploads';
import path from 'path';
import fs from 'fs-extra';
import BusBoy from 'busboy';
//import crypto from 'crypto';
const router = express.Router();

router.get('', isAdmin, (req, res, next) => {
  let usage = false;
  if (req.query.u === '1') {
    usage = true;
  }
  listItems(req.siteId, req.query.type, usage)
    .then((data) => res.json(data))
    .catch((error) => next(error));
});

router.get('/credits', (req, res, next) => {
  credits(req.siteId)
    .then((data) => res.json(data))
    .catch((err) => next(err));
});

router.get('/search', isAdmin, (req, res, next) => {
  search(req.siteId, req.query.type, req.query.q)
    .then((data) => res.json(data))
    .catch((error) => next(error));
});

router.get('/search/mw', isAdmin, (req, res, next) => {
  searchImages(req.query.q, req.query.page)
    .then((data) => {
      res.json(data);
    })
    .catch((err) => next(err));
});

router.post('/import/mediawiki', isAdmin, (req, res, next) => {
  importImage(req.siteId, req.body)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => next(err));
});

router.get('/:id(\\d+)', isAdmin, (req, res, next) => {
  getAsset(req.siteId, req.params.id, true)
    .then((asset) => res.json(asset))
    .catch((err) => next(err));
});

router.post('/:id(\\d+)', isAdmin, (req, res, next) => {
  const asset = req.body;

  asset.id = req.params.id;

  saveAsset(req.siteId, asset)
    .then((newAsset) => res.json(newAsset))
    .catch((err) => next(err));
});

router.delete('/:id(\\d+)', isAdmin, (req, res, next) => {
  getAsset(req.siteId, req.params.id, true).then((asset) => {
    if (!asset) {
      res.status(404).json(null);
      return;
    }
    if (asset.usage.length > 0) {
      res.status(409).send('Asset can not be deleted because it is in use.');
    } else {
      deleteAsset(req.siteId, asset)
        .then((siteOnly) => {
          if (!siteOnly) {
            deleteFiles(asset);
          }
          res.json(true);
        })
        .catch((err) => next(err));
    }
  });
});

router.post('/:id(\\d+)/sites', isAdmin, (req, res, next) => {
  const addOnly = req.query.add === 'true';
  saveSites(req.siteId, req.params.id, req.body, addOnly)
    .then((data) => res.json(data))
    .catch((err) => next(err));
});

router.post('/upload', isAdmin, (req, res, next) => {
  const busboy = new BusBoy({
    headers: req.headers,
    // 2 megs
    highWaterMark: 2 * 1024 * 1024,
    limits: {
      // 50 megs
      fileSize: 1024 * 1024 * 50,
      files: 1,
    },
  });

  const asset = {};
  busboy.on('file', function (fieldName, file, filename, encoding, mimetype) {
    asset.name = generateFilename(filename);
    if (mimetype.indexOf('image') >= 0) {
      asset.type = 'd';
      asset.subtype = 'i';
    } else if (mimetype.indexOf('video') >= 0) {
      asset.type = 'd';
      asset.subtype = 'v';
    } else if (mimetype.indexOf('audio') >= 0) {
      asset.type = 'a';
      asset.subtype = null;
    } else {
      res.status(400).send('Invalid file type "' + mimetype + '"');
    }

    const fstream = fs.createWriteStream(path.join(uploadDest, asset.name));

    file.pipe(fstream);
  });

  busboy.on('field', function (key, value) {
    if (['type', 'subtype', 'name'].indexOf(key) >= 0) {
      // Can't be modified in post.
      return;
    }
    if (key === 'id') {
      getAsset(req.siteId, value)
        .then((oldAsset) => {
          deleteFiles(oldAsset);
        })
        .catch((err) => next(err));
    }
    asset[key] = value;
  });

  busboy.on('finish', function () {
    const fileName = path.join(uploadDest, asset.name);

    let promise = Promise.resolve(asset);

    if (asset.subtype === 'i') {
      promise = processImage(fileName, asset);
    } else if (asset.subtype === 'v') {
      promise = processVideo(fileName, asset);
    }

    promise
      .then((processedAsset) => {
        saveAsset(req.siteId, processedAsset)
          .then((newAsset) => {
            res.json(newAsset);
          })
          .catch((err) => {
            next(err);
          });
      })
      .catch((err) => {
        next(err);
      });
  });

  req.pipe(busboy);
});

export default router;
