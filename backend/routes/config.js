import express from 'express';
import { getAllSettings, saveSettings } from '../db/settings';
import { getTranslations } from '../db/translation';
import { listAll as listAllSessonCats } from '../db/lesson_category';
import { listForLang, STATE_PUBLSISHED, STATE_DRAFT } from '../db/content_page';
import { getContactPage } from '../db/contact';
import { listAllLicenses, listUsedLicenes } from '../db/license';
import isAuthenticated from '../lib/isAuthenticated';
const router = express.Router();

router.get('/:lang', async (req, res, next) => {
  let settings = {},
    licenses,
    translations,
    lessonCategories,
    pages,
    enableContact = false;
  const queries = [];
  queries.push(
    getAllSettings(req.siteId)
      .then((result) => (settings = result))
      .catch((err) => next(err))
  );

  if (req.adminUser && req.adminUser.id) {
    // If we're logged in show all licenses in case this is the admin UI.
    queries.push(
      listAllLicenses()
        .then((res) => (licenses = res))
        .catch((err) => next(err))
    );
  } else {
    queries.push(
      listUsedLicenes(req.siteId)
        .then((res) => (licenses = res))
        .catch((err) => next(err))
    );
  }

  queries.push(
    getTranslations(req.siteId, req.params.lang)
      .then((rows) => (translations = rows))
      .catch((err) => next(err))
  );

  queries.push(
    listAllSessonCats(req.siteId)
      .then((rows) => (lessonCategories = rows))
      .catch((err) => next(err))
  );

  let state = STATE_PUBLSISHED;
  if (req.adminUser && req.adminUser.id) {
    state = STATE_DRAFT;
  }

  queries.push(
    listForLang(req.siteId, req.params.lang, state, true)
      .then((rows) => (pages = rows))
      .catch((err) => next(err))
  );

  queries.push(
    getContactPage(req.siteId)
      .then((contact) => (enableContact = contact !== false))
      .catch((err) => next(err))
  );

  try {
    await Promise.all(queries);
  } catch (e) {
    next(e);
    return;
  }

  let username = undefined;
  if (req.adminUser && req.adminUser.id) {
    username = req.adminUser.email;
  }
  settings.enableContact = enableContact;
  settings.licenses = licenses;

  res.json({
    settings: settings,
    lesson_categories: lessonCategories,
    pages: pages,
    translations: translations,
    username: username,
  });
});

router.get('', isAuthenticated, async (req, res) => {
  const settings = await getAllSettings(req.siteId, false);
  res.json(settings);
});

router.post('', isAuthenticated, (req, res, next) => {
  saveSettings(req.siteId, req.body)
    .then((data) => res.json(data))
    .catch((err) => next(err));
});

export default router;
