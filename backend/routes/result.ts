import { resolveFBUser, isAdmin } from '../lib/isAuthenticated';
import { getFireBaseApp } from '../config/firebaseAdmin';
import { Router, Request, Response, NextFunction } from 'express';
import { FORBIDDEN, INTERNAL_SERVER_ERROR } from 'http-status-codes';
import {
  countUsers,
  getItemForResult,
  getQtyResultsForUser,
  getResultsForUser,
  incrementSummary,
  listUsers,
  saveResult,
} from '../db/result';
const router = Router();

router.post('', async (req: Request, res: Response, next: NextFunction) => {
  const fbUser = await resolveFBUser(req);
  if (!fbUser) {
    res.sendStatus(FORBIDDEN);
    return;
  }

  const data = req.body as {
    takeId: number;
    itemId: number;
    isCorrect: boolean;
  };

  try {
    const item = await getItemForResult(req.siteId, data.itemId);

    if (item) {
      await incrementSummary(req.siteId, fbUser.uid, data.takeId, item.activity_id);
      await saveResult(req.siteId, fbUser.uid, data.takeId, item, data.isCorrect);
    }

    res.json(null);
  } catch (e) {
    next(e);
  }
});

router.get('', isAdmin, async (req: Request, res: Response) => {
  const page = req.query.page ? parseFloat(req.query.page as string) : 0,
    qtyPerpage = req.query.qtyPerPage ? parseFloat(req.query.qtyPerPage as string) : 50;

  const [qtyUsers, list] = await Promise.all([
    countUsers(req.siteId),
    listUsers(req.siteId, page, qtyPerpage),
  ]);

  if (qtyUsers === 0 || list.length === 0) {
    res.json({ data: [], qtyRows: 0 });
    return;
  }

  const criteria = list.map((u) => ({ uid: u.user_id }));

  const app = await getFireBaseApp(req.siteId);
  if (!app) {
    res.json({ data: [], qtyRows: 0 });
    return;
  }

  const users = await app.auth().getUsers(criteria);

  const data = [];

  for (let i = 0; i < list.length; i++) {
    const row = list[i],
      user = users.users[i];

    data.push({
      uid: row.user_id,
      started: row.started,
      name: user ? user.displayName : '',
      email: user ? user.email : '',
      photoUrl: user ? user.photoURL : '',
    });
  }

  res.json({
    data,
    qtyRows: qtyUsers,
  });
});

router.get(
  '/:uid',
  isAdmin,
  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const app = await getFireBaseApp(req.siteId);
    if (!app) {
      res.sendStatus(INTERNAL_SERVER_ERROR);
      return;
    }

    const page = req.query.page ? parseFloat(req.query.page as string) : 0,
      qtyPerpage = req.query.qtyPerPage ? parseFloat(req.query.qtyPerPage as string) : undefined;

    try {
      const [user, results, qty] = await Promise.all([
        app.auth().getUser(req.params.uid),
        getResultsForUser(req.siteId, req.params.uid, page, qtyPerpage),
        getQtyResultsForUser(req.siteId, req.params.uid),
      ]);

      res.json({
        data: results,
        qtyRows: qty,
        user: {
          name: user.displayName,
          email: user.email,
          photoUrl: user.photoURL,
        },
      });
    } catch (e) {
      next(e);
    }
  }
);

export default router;
