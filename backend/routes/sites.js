import { Router } from 'express';
import { sites } from '../config/sites';
import isAuthenticated from '../lib/isAuthenticated';

const router = Router();

router.get('', isAuthenticated, (req, res, next) => {
  res.json(sites);
});

export default router;
