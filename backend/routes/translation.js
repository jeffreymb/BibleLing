import {
  copyBetweenSites,
  deleteTranslation,
  getLanguagesForString,
  getTranslations,
  saveTranslation,
  saveTranslationForLang,
} from '../db/translation';
import isAuthenticated from '../lib/isAuthenticated';
import { Router } from 'express';

const router = Router();

router.get('/:lang', (req, res, next) => {
  getTranslations(req.siteId, req.params.lang)
    .then((rows) => res.json(rows))
    .catch((err) => next(err));
});

router.get('/string/:string_id', isAuthenticated, (req, res, next) => {
  getLanguagesForString(req.siteId, req.params.string_id)
    .then((rows) => res.json(rows))
    .catch((err) => next(err));
});

router.post('/string/:string_id', isAuthenticated, (req, res, next) => {
  saveTranslation(req.siteId, req.params.string_id, req.body)
    .then(() => res.json(true))
    .catch((error) => next(error));
});

router.delete('/string/:string_id', isAuthenticated, (req, res, next) => {
  deleteTranslation(req.siteId, req.params.string_id)
    .then(() => res.json(true))
    .catch((err) => next(err));
});

router.post('/:lang', isAuthenticated, (req, res, next) => {
  saveTranslationForLang(req.siteId, req.params.lang, req.body)
    .then((result) => res.json(result))
    .catch((error) => next(error));
});

router.post('/:lang/copy_from/:site_id', isAuthenticated, (req, res, next) => {
  copyBetweenSites(req.siteId, req.params.site_id, req.params.lang)
    .then((result) => res.json(result))
    .catch(next);
});

export default router;
