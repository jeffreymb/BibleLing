import axios from 'axios';
import dgb from 'debug';
import fs from 'fs';
import path from 'path';
import uploadDest from '../config/uploads';
import { generateFilename, processImage, processVideo } from './asset';
import { saveAsset } from '../db/asset';
import { saveLicense } from '../db/license';

const debug = dgb('backend:asset:mediawiki');

export function searchImages(searchTerm, page) {
    if (!page) {
        page = 0;
    }
    const offset = page * 50,
        searchString = encodeURIComponent(searchTerm),
        url = `https://commons.wikimedia.org/w/api.php?action=query&\
            generator=search&gsrnamespace=6&\
            gsrsearch=${searchString}&\
            gsrlimit=50&gsroffset=${offset}&\
            prop=imageinfo&iiprop=url|size|extmetadata|mediatype&\
            iiurlwidth=1024&format=json`;
    debug(`searching for "${searchTerm}" with offset ${offset}`);

    return axios.get(url, { timeout: 25000 }).then((res) => {
        const images = [],
            pages = res.data.query ? res.data.query.pages : {},
            keys = Object.keys(pages);
        debug(`found ${keys.length} items`);

        for (let i = 0; i < keys.length; i++) {
            const p = pages[keys[i]],
                info = p.imageinfo[0],
                img = {
                    name: info.url,
                    title: p.title,
                    type: 'd',
                    subtype: null,
                    metadata: {
                        ratio: Math.round((info.thumbwidth / info.thumbheight) * 100) / 100,
                        width: info.thumbwidth,
                        height: info.thumbheight,
                    },
                };
            let subtype;

            if (info.extmetadata.License !== undefined) {
                img.license = info.extmetadata.License.value;
            }
            if (info.extmetadata.LicenseShortName !== undefined) {
                img.license_name = info.extmetadata.LicenseShortName.value;
            }

            switch (info.mediatype) {
                case 'BITMAP':
                    // So that the thumbnail is shown instead of the full size image.
                    img.name = info.thumburl;
                    img.subtype = 'i';
                    break;
                case 'VIDEO':
                    img.subtype = 'v';
                    img.metadata.poster = info.thumburl;
                    img.metadata.sizes = [
                        {
                            width: info.thumbwidth,
                            url: info.url,
                        },
                    ];
                    break;
                default:
                    subtype = 'x';
                    debug(`UNKNOWN media type ${info.mediatype}`);
            }

            if (subtype !== 'x') {
                images.push(img);
            }
        }

        // return {pages, images, raw: res.data};
        debug(`returned ${images.length} items`);
        return images;
    });
}

export async function importImage(siteId, image) {
    debug(`importing "${JSON.stringify(image)}"`);

    const assetName = generateFilename(image.name),
        destFile = path.join(uploadDest, assetName);
    let response;
    try {
        response = await axios({
            method: 'get',
            url: image.name,
            responseType: 'stream',
        });
    } catch (err) {
        debug(`error fetching asset "${err}"`);
        return Promise.reject(err);
    }

    const writer = fs.createWriteStream(destFile);
    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', () => {
            console.log(`finished downloading image`);
            const asset = {
                name: assetName,
                caption: image.caption,
                type: 'd',
                subtype: image.subtype,
                license: image.license,
                credits_source: 'Wikipedia Commons',
            };
            const promises = [Promise.resolve()];

            if (asset.subtype === 'i') {
                promises.push(processImage(destFile, asset));
            } else if (asset.subtype === 'v') {
                promises.push(processVideo(destFile, asset));
            }

            if (image.license && image.license_name) {
                promises.push(
                    saveLicense({
                        id: image.license,
                        name: image.license_name,
                    })
                );
            }

            Promise.all(promises)
                .then((metadata) => {
                    // asset.metadata = metadata;

                    saveAsset(siteId, asset)
                        .then((newAsset) => {
                            resolve(newAsset);
                        })
                        .catch((err) => reject(err));
                })
                .catch((err) => {
                    reject(err);
                    debug(`Error processing file "${err}"`);
                });
        });
        writer.on('error', (err) => {
            reject(err);

            debug(`Error writing file "${err}"`);
        });
    });
}
