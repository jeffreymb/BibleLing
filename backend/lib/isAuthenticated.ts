import { Request, Response, NextFunction } from 'express';
import { getFireBaseApp } from '../config/firebaseAdmin';
import admin from 'firebase-admin';
import { FORBIDDEN } from 'http-status-codes';

/**
 * Populate the request.fbUser property with the cliam from FireBase.
 * @param req
 * @return
 */
export async function resolveFBUser(req: Request): Promise<admin.auth.DecodedIdToken | null> {
  if (req.fbUser) {
    return req.fbUser;
  }

  const token = req.header('authorization');

  if (token) {
    const splitToken = token.split(' ');
    if (splitToken.length !== 2) {
      return null;
    }
    const tokenToken = splitToken[1];

    const app = await getFireBaseApp(req.siteId);
    if (app) {
      try {
        const claims = await app.auth().verifyIdToken(tokenToken);
        if (claims) {
          return claims;
        }
      } catch (e) {
        // Ignore!
      }
    }
  }

  return null;
}

export function isAdmin(req: Request, res: Response, next: NextFunction): void {
  if (req.adminUser && req.adminUser.id) {
    return next();
  }
  res.sendStatus(FORBIDDEN);
}

export default isAdmin;
