module.exports = {
  insertTranslations(db, translations, lang) {
    return new Promise((resolve) => {
      db.all('SELECT DISTINCT site_id FROM translations', [], (err, results) => {
        if (err) {
          throw err;
        }
        const keys = Object.keys(translations);
        // let args = [];
        const promises = [];

        for (let s = 0; s < results.length; s++) {
          const siteId = results[s].site_id;

          for (let t = 0; t < keys.length; t++) {
            const k = keys[t],
              v = translations[k];
            promises.push(
              db.insert(
                'translations',
                ['lang', 'site_id', 'string_id', 'text'],
                [lang, siteId, k, v]
              )
            );
          }
        }

        Promise.all(promises).then(() => {
          resolve();
        });
      });
    });
  },
  deleteTranslations(db, translations, lang) {
    const strings = Object.keys(translations).join("','");

    return db.runSql(
      `delete from translations where lang = '${lang}' and string_id in ('${strings}')`
    );
  },
};
