import path from 'path';
import fs from 'fs-extra';
import ffmpeg from 'ffmpeg';
import sharp from 'sharp';
import mime from 'mime-types';
import sizeOf from 'image-size';
import uploadDest from '../config/uploads';
import crypto from 'crypto';

// So that the original files are closed after processing.
sharp.cache(false);

const buildOriginalName = function (fileName) {
    const parts = fileName.split('.'),
        name = parts[0],
        ext = parts[1];
    return name + '_org.' + ext;
};

const findOriginalName = function (filename) {
    const principalParts = filename.split('.'),
        name = principalParts[0],
        nameParts = name.split('_');
    if (nameParts.length > 1) {
        // We have an original extension embeded in the filename.
        return buildOriginalName(`${nameParts[0]}.${nameParts[1]}`);
    }

    return buildOriginalName(filename);
};

export function generateFilename(orgName) {
    const name = crypto.randomBytes(15).toString('hex'),
        extension = orgName.split('.')[1].toLowerCase();
    return `${name}.${extension}`;
}

export function deleteFiles(asset) {
    const del = [asset.name];

    if (asset.subtype === 'i') {
        del.push(findOriginalName(asset.name));
    } else if (asset.subtype === 'v') {
        if (asset.metadata) {
            del.push(asset.metadata.poster);
            for (let sizeId = 0; sizeId < asset.metadata.sizes.length; sizeId++) {
                const size = asset.metadata.sizes[sizeId];

                del.push(size.name);
            }
        }
    }

    for (let i = 0; i < del.length; i++) {
        if (del[i].length > 0) {
            const fullPath = path.join(uploadDest, del[i]);
            fs.stat(fullPath, (err, stat) => {
                if (err) {
                    return;
                }
                fs.unlink(fullPath, (err) => {
                    if (err) {
                        // Basically just swallow the error.
                        console.log(err);
                    }
                });
            });
        }
    }
}

export function processImage(sourcePath, asset) {
    return new Promise((resolve, reject) => {
        sizeOf(sourcePath, (err, dimens) => {
            if (err) {
                reject(err);
                return;
            }
            const mimeType = mime.lookup(sourcePath),
                // ~1 megapixel.
                MAX_IMAGE_RES = 1050000,
                ratio = Math.round((dimens.width / dimens.height) * 100) / 100,
                orgRes = dimens.width * dimens.height;
            if (orgRes > MAX_IMAGE_RES || mimeType !== 'image/jpeg') {
                let width = dimens.width,
                    height = dimens.height;
                if (orgRes > MAX_IMAGE_RES) {
                    if (ratio > 1) {
                        width = 1024;
                        height = Math.round(width / ratio);
                    } else {
                        height = 1024;
                        width = Math.round(height * ratio);
                    }
                }

                const orgName = path.join(uploadDest, buildOriginalName(asset.name));
                let targetPath = sourcePath;

                if (mimeType !== 'image/jpeg') {
                    const parts = asset.name.split('.'),
                        name = parts[0],
                        ext = parts[1];
                    // Add the original extension to the name, so we can find it to delete it.
                    asset.name = `${name}_${ext}.jpg`;

                    // Full path for the processed image.
                    targetPath = path.join(uploadDest, asset.name);
                }

                fs.rename(sourcePath, orgName, (err) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    sharp(orgName)
                        .resize(width, height)
                        .jpeg({
                            quality: 90,
                            progressive: true,
                        })
                        .toFile(targetPath)
                        .then(() => {
                            asset.metadata = {
                                width: width,
                                height: height,
                                ratio: ratio,
                            };
                            resolve(asset);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            } else {
                asset.metadata = {
                    width: dimens.width,
                    height: dimens.height,
                    ratio: ratio,
                };
                resolve(asset);
            }
        });
    });
}

export function processVideo(sourcePath, asset) {
    return new Promise((resolve, reject) => {
        const name = path.basename(sourcePath).split('.')[0],
            tasks = [],
            metadata = {
                sizes: [],
            };
        tasks.push(
            new Promise((posterResolve, posterReject) => {
                const process = new ffmpeg(sourcePath);
                process.then(
                    (video) => {
                        metadata.duration = video.metadata.duration.seconds;
                        metadata.ratio = Math.round(video.metadata.video.aspect.value * 100) / 100;
                        const width = Math.round(768 * metadata.ratio);
                        metadata.width = width;
                        metadata.height = 768;

                        video
                            .fnExtractFrameToJPG(uploadDest, {
                                number: 1,
                                size: width + 'x768',
                                file_name: name,
                            })
                            .then(
                                () => {
                                    fs.rename(
                                        path.join(uploadDest, name + '_1.jpg'),
                                        path.join(uploadDest, name + '_poster.jpg'),
                                        (err) => {
                                            if (err) {
                                                posterReject(err);
                                                return;
                                            }
                                            metadata.poster = name + '_poster.jpg';
                                            posterResolve();
                                        }
                                    );
                                },
                                (err) => {
                                    posterReject(err);
                                }
                            );
                    },
                    (err) => {
                        posterReject(err);
                    }
                );
            })
        );

        const sizes = [
            {
                name: name + '_small.mp4',
                width: 325,
                bitRate: 250,
            },
            {
                name: name + '_large.mp4',
                width: 1024,
                bitRate: 1024,
            },
        ];

        for (let i = 0; i < sizes.length; i++) {
            tasks.push(
                new Promise((sizeResolve, sizeReject) => {
                    const proc = new ffmpeg(sourcePath),
                        config = sizes[i];
                    proc.then(
                        (vid) => {
                            let width = config.width,
                                height = Math.round(width / vid.metadata.video.aspect.value),
                                dimens = config.width + 'x' + height;
                            if (height > vid.metadata.video.resolution.h) {
                                // Don't upsize video.
                                width = vid.metadata.video.resolution.w;
                                height = vid.metadata.video.resolution.h;
                                dimens = width + 'x' + height;
                            }
                            vid.setDisableAudio()
                                .setVideoFormat('mp4')
                                .setVideoSize(dimens)
                                .setVideoFrameRate(25)
                                // .setVideoDuration(5)
                                .save(path.join(uploadDest, config.name), (err) => {
                                    if (err) {
                                        sizeReject(err);
                                        return;
                                    }
                                    metadata.sizes.push({
                                        width: width,
                                        name: config.name,
                                    });
                                    sizeResolve();
                                });
                        },
                        (err) => {
                            sizeReject(err);
                        }
                    );
                })
            );
        }

        Promise.all(tasks)
            .then((results) => {
                asset.metadata = metadata;
                resolve(asset);
            })
            .catch((err) => {
                reject(err);
            });
    });
}
