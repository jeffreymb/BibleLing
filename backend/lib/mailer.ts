import sgMail from '@sendgrid/mail';
import { MailDataRequired } from '@sendgrid/helpers/classes/mail';

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

export async function sendMessage(msgObj: MailDataRequired): Promise<void> {
    await sgMail.send(msgObj);
}
