import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import bcrypt from 'bcryptjs';
import { getByEmail, getById } from '../db/user';
import debugFactory from 'debug';
const debug = debugFactory('backend:database:users');

passport.use(
  new LocalStrategy(
    {
      passReqToCallback: true,
    },
    async function (req, username, password, done) {
      const user = await getByEmail(req.siteId, username);
      if (!user) {
        debug(`invalid username for "${username}"`);
        done(null, false);
        return;
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          debug(`"${username}" successfully logged in`);
          done(null, user);
        } else {
          debug(`bad username or password for "${username}"`);
          done(null, false);
        }
      });
    }
  )
);

passport.serializeUser(function (user, next) {
  next(null, user.id);
});

passport.deserializeUser(async function (id, next) {
  const user = await getById(id);
  next(null, user);
});

export default passport;
