interface InputOptionConfig {
  label: string;
  type: string;
  description: string;
  default: string | boolean | number;
  inputStyle?: {
    [key: string]: string;
  };
}
interface ActvityTypeConfig {
  title: string;
  description: string;
  options: {
    [key: string]: InputOptionConfig;
  };
  pages: {
    image: boolean | number;
    audio: boolean | number;
    content: boolean;
    html?: boolean;
    timing?: boolean;
    options?: {
      [key: string]: InputOptionConfig | boolean | undefined | number;
      image?: boolean | number;
      audio?: boolean | number;
      content?: boolean;
    };
  };
}

declare const data: {
  [key: string]: ActvityTypeConfig;
};

export default data;
