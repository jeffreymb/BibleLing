import admin from 'firebase-admin';
import { getSetting } from '../db/settings';
import rollbar from './rollbar';

const apps: { [key: string]: admin.app.App | null } = {};

async function initApp(siteId: string): Promise<null | admin.app.App> {
  const setting = await getSetting(siteId, 'firebaseServiceAccount');

  if (!setting) {
    return null;
  }

  const settingParsed = JSON.parse(setting);

  try {
    return admin.initializeApp(
      {
        credential: admin.credential.cert(settingParsed),
      },
      siteId
    );
  } catch (e) {
    rollbar.error(e);
  }
  return null;
}

export async function getFireBaseApp(siteId: string): Promise<null | admin.app.App> {
  if (!(siteId in apps)) {
    apps[siteId] = await initApp(siteId);
  }

  return apps[siteId];
}
