import fs from 'fs-extra';
import { debugFactory } from './debug';
const debug = debugFactory('upload_config');

const UploadDir = process.env.UPLOAD_DIR as string;

if (!UploadDir) {
  throw new Error('UPLOAD_DIR environment variable is required');
}

fs.ensureDirSync(UploadDir);

debug(`Upload dir is: ${UploadDir}`);

export default UploadDir;
