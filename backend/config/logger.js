import morgan from 'morgan';

export default function (app) {
  morgan.token('siteId', function (req) {
    return req.siteId;
  });

  app.use(morgan(':siteId :method :url :status :response-time ms - :res[content-length]'));
}
