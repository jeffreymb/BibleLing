import debug from 'debug';

export const appDebug = debug('app');
appDebug.log = console.log.bind(console);

export function debugFactory(name: string): debug.Debug {
  return (appDebug.extend(name) as unknown) as debug.Debug;
}
