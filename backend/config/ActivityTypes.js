/**
 * this file must remain a .js file because it is also used by the front end.
 * @type {string}
 */
let firstItemNotice =
  '<div class="alert alert-warning mb-0">The first row on a page is the "correct" answer.</div>';
export default {
  discovery: {
    title: 'Discovery',
    description:
      'See image, click to hear audio. <div class="alert alert-info">It is recommened to have not more than 6 items per page.</div>',
    pages: {
      image: true,
      audio: true,
      content: false,
    },
  },
  quiz: {
    title: 'Quiz',
    description: 'Hear audio and click the correct image',
    options: {
      maxShown: {
        label: 'Max Items',
        type: 'number',
        description: 'Max items to show on a single quiz page.',
        inputStyle: {
          width: '5em',
        },
        default: 6,
      },
      minShown: {
        label: 'Min Items',
        type: 'number',
        description:
          'Minimum items to show on a single quiz page. Should be more than 1 and less than Max Items.',
        inputStyle: {
          width: '5em',
        },
        default: 3,
      },
    },
    pages: {
      image: true,
      audio: true,
      content: false,
      options: {
        random: {
          label: 'Randomize',
          type: 'checkbox',
          description: 'If the images in this section should be randomized or ordered',
          default: false,
        },
      },
    },
  },
  read_select_image: {
    title: 'Read and Select Image',
    description:
      'Read a content (and optionally click to hear it read) and selct the correct image.' +
      firstItemNotice,
    pages: {
      image: true,
      audio: true,
      content: false,
      options: {
        content: true,
      },
    },
  },
  image_select_text: {
    title: 'See Image and Select Text',
    description:
      'See a single image and click on the correct text, optionally with audio. Only the image on the first row will ever be shown.' +
      firstItemNotice,
    pages: {
      image: true,
      audio: true,
      content: true,
    },
  },
  multiple_choice_text: {
    title: 'Multiple Choice Text',
    description:
      'See a single text and click on the correct matching text. If audio is added, only the audio on the first item will be used.' +
      firstItemNotice,
    options: {
      randomize_pages: {
        label: 'Randomize Pages',
        type: 'checkbox',
        description: 'If selected, the page order will be randomized.',
        default: false,
      },
      text_size: {
        label: 'Default Text Size',
        type: 'text',
        description: 'Default text size of the "correct" answer.',
        inputStyle: {
          width: '5em',
        },
      },
    },
    pages: {
      image: false,
      audio: true,
      content: true,
      options: {
        content: true,
        text_size: {
          label: 'Text Size',
          type: 'text',
          description: 'Text size of the "correct" answer (leave blank for default).',
          default: 1,
          inputStyle: {
            width: '5em',
          },
        },
      },
    },
  },
  match_image_text: {
    title: 'Match Image and Text',
    description: '',
    pages: {
      image: true,
      audio: false,
      content: true,
    },
  },
  text: {
    title: 'Text document',
    description: 'A page of text',
    pages: {
      html: true,
    },
  },
  sequence: {
    title: 'Sequence',
    description: 'Images and audio that auto advance like a slideshow/video.',
    pages: {
      image: 10,
      audio: 10,
      content: true,
      options: {
        before: {
          label: 'Delay Before',
          type: 'number',
          description: 'Milliseconds after showing image to play audio.',
          inputStyle: {
            width: '6em',
          },
          default: 500,
        },
        between: {
          label: 'Delay Between',
          type: 'number',
          description: 'Milliseconds between audio on the same image.',
          inputStyle: {
            width: '6em',
          },
          default: 600,
        },
        after: {
          label: 'Delay After',
          type: 'number',
          description: 'Milliseconds after audio finishes before progressing to next image.',
          inputStyle: {
            width: '6em',
          },
          default: 1500,
        },
      },
    },
  },
  audio_sequence: {
    title: 'Audio Sequence',
    description:
      'Sequence of images that automatically advance at specified timings.' +
      '<div class="alert alert-info">If the first image has a "show at seconds" value greater than zero, that amount of time will be skipped at the beginning of the audio file.</div>',
    pages: {
      image: true,
      audio: false,
      content: true,
      timing: true,
      options: {
        audio: true,
      },
    },
  },
};
