import db from './main';
import { query } from './database';
import { getPages, updatePages } from './page';
import { deleteTranslation } from './translation';

export async function getActivity(activityId) {
  const results = await query(
    'select a.id, a.lesson_id, a.type, a.config from activities a where a.id = ?',
    [activityId]
  );

  const activity = results[0];
  if (!activity) {
    return null;
  }
  if (activity.config === null) {
    activity.config = {};
  } else {
    activity.config = JSON.parse(activity.config);
  }

  return await new Promise((resolve, reject) => {
    getPages(activityId)
      .then((pages) => {
        activity.pages = pages;
        resolve(activity);
      })
      .catch((err) => reject(err));
  });
}

export async function getNextOrder(lessonId) {
  const sql = 'select coalesce(max(`order`), 0) + 1 next_order from activities where lesson_id = ?';

  const results = await query(sql, [lessonId]);

  return results[0].next_order;
}

export function updateActivity(activity) {
  return new Promise((resolve, reject) => {
    if (!activity.id) {
      getNextOrder(activity.lesson_id)
        .then((order) => {
          const sql =
            'insert into activities (lesson_id, type, `order`, config) values (?, ?, ?, ?)';

          db.query(sql, [activity.lesson_id, activity.type, order, '{}'], (err, result) => {
            if (err) {
              reject(err);
            } else {
              activity.id = result.insertId;
              activity.config = {};
              updatePages(activity.id, activity.pages)
                .then(() => {
                  getActivity(activity.id)
                    .then((freshActivity) => resolve(freshActivity))
                    .catch((err) => reject(err));
                })
                .catch((err) => reject(err));
            }
          });
        })
        .catch((err) => reject(err));
    } else {
      const sql = 'update activities set type = ?, config = ? WHERE id = ?';

      db.query(sql, [activity.type, JSON.stringify(activity.config), activity.id], function (err) {
        if (err) {
          reject(err);
        } else {
          updatePages(activity.id, activity.pages)
            .then(() => {
              getActivity(activity.id)
                .then((freshActivity) => resolve(freshActivity))
                .catch((err) => reject(err));
            })
            .catch((err) => reject(err));
        }
      });
    }
  });
}

export async function saveOrder(activities) {
  let sql = 'update activities set `order` = ? where id = ?',
    queries = [];
  for (let i = 0; i < activities.length; i++) {
    queries.push(query(sql, [activities[i].order, activities[i].id]));
  }

  await Promise.all(queries);

  return queries.length;
}

export async function deleteActivity(activity, siteId) {
  let sql = 'delete from activities where id = ?';

  await query(sql, [activity.id]);

  await deleteTranslation(siteId, `_activity_${activity.id}`);

  return true;
}
