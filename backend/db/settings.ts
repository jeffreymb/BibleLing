import { query } from './database';
import { LicenseInterface } from './license';

interface SettingInterface {
  id: string;
  site_id: string;
  admin_only: boolean;
  settings: unknown;
}

interface LicenseSettings extends SettingInterface {
  settings: Array<LicenseInterface>;
}

interface SettingsList {
  [key: string]: SettingInterface | undefined;
  licenses?: LicenseSettings;
}

enum AdminOnlySettings {
  firebaseServiceAccount = 'firebaseServiceAccount',
}

export async function getAllSettings(
  siteId: string,
  excludeAdminOnly = true
): Promise<SettingsList> {
  let where = 'site_id = ?';
  if (excludeAdminOnly) {
    where += ' and admin_only = 0';
  }
  const results = await query<Array<SettingInterface>>(`select * from settings where ${where}`, [
    siteId,
  ]);

  const settings: SettingsList = {};
  for (let i = 0; i < results.length; i++) {
    const row: SettingInterface = results[i];

    settings[row.id] = JSON.parse(row.settings as string);
  }

  return settings;
}

export async function getSetting(siteId: string, settingId: string): Promise<string | undefined> {
  const results = await query<Array<SettingInterface>>(
    'select * from settings where site_id = ? and id = ?',
    [siteId, settingId]
  );

  if (results.length !== 1) {
    return undefined;
  }

  return JSON.parse(results[0].settings as string);
}

export async function saveSettings(
  siteId: string,
  settings: { [name: string]: unknown }
): Promise<SettingsList> {
  const args: Array<unknown> = [],
    keys = Object.keys(settings);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i],
      value = settings[key];
    args.push(key);
    args.push(siteId);
    args.push(key in AdminOnlySettings);
    args.push(JSON.stringify(value));
  }

  const q = new Array(keys.length).fill('(?,?,?,?)').join(','),
    sql = 'replace into settings (id, site_id, admin_only, settings) values ' + q;
  await query(sql, args);

  return await getAllSettings(siteId);
}
