import mysql from 'mysql';
import util from 'util';
import { appDebug } from '../config/debug';

const debug = appDebug.extend('mysql');

const pool: mysql.Pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

export interface InsertResult {
  insertId: number;
  affectedRows: number;
}

const promisifiedQuery = (util.promisify(pool.query).bind(pool) as unknown) as <R>(
  query: string,
  args?: Array<unknown>
) => Promise<R>;

export async function query<R>(query: string, args?: Array<unknown>): Promise<R> {
  return promisifiedQuery<R>(query, args);
}

pool.query = (query as unknown) as mysql.QueryFunction;
debug('init complete');
export default pool;
