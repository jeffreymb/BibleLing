import { InsertResult, query } from './database';
import { deleteTranslation } from './translation';
export const STATE_DRAFT = 0,
  STATE_PUBLISHED = 1;

export enum LessonStates {
  DRAFT = 0,
  PUBLISHED = 1,
}

interface Lesson {
  id: number;
  order: number;
  state: LessonStates;
  category_id: number;
  activities: Array<Activity>;
}

interface Activity {
  id: number;
  lesson_id: number;
  order: number;
  type: string;
  config: {};
}

export async function getLessons(siteId: string, showDraft: boolean): Promise<Array<Lesson>> {
  const rows = await query<
    Array<Lesson & { a_id: number | null; a_order: number; type: string; config: string }>
  >(
    'select l.id, l.`order`, l.state, l.category_id, a.id a_id, a.`order` a_order, a.type, a.config \
    from lessons l \
        left join activities a on a.lesson_id = l.id \
    where l.site_id = ? and l.state >= ?\
    order by l.`order`, a.`order`',
    [siteId, showDraft ? LessonStates.DRAFT : LessonStates.PUBLISHED]
  );

  const data: Array<Lesson> = [],
    activityMap: { [key: number]: number } = {};

  for (let lkey = 0; lkey < rows.length; lkey++) {
    const row = rows[lkey];
    if (activityMap[row.id] === undefined) {
      data.push({
        id: row.id,
        order: row.order,
        state: row.state,
        category_id: row.category_id,
        activities: [],
      });
      activityMap[row.id] = data.length - 1;
    }

    if (row.a_id !== null) {
      data[activityMap[row.id]].activities.push({
        id: row.a_id,
        lesson_id: row.id,
        order: row.a_order,
        type: row.type,
        config: JSON.parse(row.config),
      });
    }
  }
  return data;
}

export async function saveLesson(siteId: string, lesson: Lesson): Promise<number> {
  if (lesson.id) {
    const sql = 'update lessons set state = ?, category_id = ? where id = ? and site_id = ?';

    const newLesson = await query<{ id: number }>(sql, [
      lesson.state,
      lesson.category_id,
      lesson.id,
      siteId,
    ]);
    return newLesson.id;
  } else {
    const order = await query<{ new_order: number }>(
      'select coalesce(max(`order`), 0) + 1 new_order from lessons where site_id = ?',
      [siteId]
    );

    const result = await query<InsertResult>(
      'insert into lessons (`order`, state, category_id, site_id) values (?, ?, ?, ?)',
      [order.new_order, STATE_DRAFT, null, siteId]
    );
    return result.insertId;
  }
}

export async function saveOrder(
  siteId: string,
  lessons: Array<{ order: number; id: number }>
): Promise<number> {
  const sql = 'update lessons set `order` = ? where id = ? and site_id = ?';

  const queryQueue = [];
  for (let i = 0; i < lessons.length; i++) {
    queryQueue.push(query(sql, [lessons[i].order, lessons[i].id, siteId]));
  }

  await Promise.all(queryQueue);

  return queryQueue.length;
}

export async function deleteLesson(siteId: string, lessonId: number): Promise<boolean> {
  const activitiesRes = await query<{ length: number }>(
    'select id \
  from activities a \
  where a.lesson_id = ? \
  limit 1',
    [lessonId]
  );

  if (activitiesRes.length > 0) {
    // We can't delete a lesson that has activities
    return false;
  }

  const result = await query<InsertResult>('delete from lessons where id = ? and site_id = ?', [
    lessonId,
    siteId,
  ]);

  const success = result.affectedRows === 1;

  if (success) {
    await deleteTranslation(siteId, `_lesson_${lessonId}`);
    await deleteTranslation(siteId, `_lesson_credits_${lessonId}`);
  }

  return success;
}
