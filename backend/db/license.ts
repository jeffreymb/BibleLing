import { query } from './database';

export interface LicenseInterface {
    id: number;
    name: string;
}

export async function listAllLicenses(): Promise<Array<LicenseInterface>> {
    return query('SELECT id, name FROM licenses ORDER BY id ASC');
}

export async function listUsedLicenes(siteId: string): Promise<Array<LicenseInterface>> {
    return query(
        'SELECT DISTINCT l.id, l.name \
        FROM licenses l \
            JOIN assets a on l.id = a.license \
            JOIN asset_sites `as` on a.id = `as`.asset_id\
        WHERE as.site_id = ?\
        ORDER BY id ASC',
        [siteId]
    );
}

export async function saveLicense(license: LicenseInterface): Promise<void> {
    await query('INSERT INTO licenses (id, name) VALUES (?, ?) \
        ON DUPLICATE KEY UPDATE name = VALUES(name)', [
        license.id,
        license.name,
    ]);
}

export async function deleteLicense(id: number): Promise<boolean> {
    try {
        await query('DELETE FROM licenses WHERE id = ?', [id]);
        return true;
    } catch (e) {
        // couldn't be deleted, likely because it is in use.
        return false;
    }
}
