import { InsertResult, query } from './database';

export interface UserInterface {
  id: number;
  site_id: string;
  email: string;
  password: string;
}

export async function getById(id: number): Promise<UserInterface | false> {
  const results = await query<Array<UserInterface>>('select * from users where id like ?', [id]);
  if (results.length === 0) {
    return false;
  } else {
    return results[0];
  }
}

export async function getByEmail(siteId: string, email: string): Promise<UserInterface | false> {
  const results = await query<UserInterface[]>('select * from users where site_id like ? and email like ?', [
    siteId,
    email,
  ]);

  if (!results) {
    return false;
  } else {
    return results[0];
  }
}

export async function saveUser(siteId: string, user: UserInterface): Promise<UserInterface> {
  if (user.id) {
    const args: Array<string | number> = [user.email];
    let passPart = '';

    if (user.password) {
      passPart = ', password = ? ';
      args.push(user.password);
    }

    args.push(user.id);

    const sql = `update users set email = ?${passPart} where id = ?`;

    await query<InsertResult>(sql, args);

    return getById(user.id) as Promise<UserInterface>;
  } else {
    const sql = 'insert into users (site_id, email, password) values (?, ?, ?)';

    const results = await query<InsertResult>(sql, [siteId, user.email, user.password]);

    return getById(results.insertId as number) as Promise<UserInterface>;
  }
}

export async function deleteUser(id: number): Promise<true> {
  await query('delete from users where id = ?', [id]);

  return true;
}

export async function listAll(siteId: string): Promise<Array<UserInterface>> {
  return query<Array<UserInterface>>('select id, email from users where site_id=?', [siteId]);
}
