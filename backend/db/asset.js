import { query } from './database';
import { STATE_PUBLISHED } from './lesson';

/**
 *
 * @param siteId String|Boolean String site id to filter to specific site id, boolean false to get usage for all sites.
 * @param id
 * @param detail
 * @return {Promise<any>}
 */
export async function getAsset(siteId, id, detail) {
    let select = 'select distinct a.* ',
        from = 'from assets a inner join asset_sites `as` on a.id = `as`.asset_id ',
        where = 'where a.id = ? ',
        groupBy = '';
    const params = [id];

    if (siteId !== false) {
        where += 'and `as`.site_id = ? ';
        params.push(siteId);
    }

    if (detail) {
        // include the select
        select += ', group_concat(json_object("site_id", sa2.site_id, "owner", sa2.owner mod 2)) as sites';
        groupBy = ' group by a.id';
        if (siteId === false) {
            select += ', ia.item_id, p.id page_id, a.id activity_id, a2.lesson_id, l.site_id ';
            from =
                'from assets a \
                        left outer join item_assets ia on ia.asset_id = a.id \
                        left outer join items i on ia.item_id = i.id \
                        left outer join pages p on i.page_id = p.id \
                        left outer join activities a2 on p.activity_id = a2.id \
                        left outer join lessons l on l.id = a2.lesson_id \
                        left join asset_sites sa2 on a.id = sa2.asset_id ';
            groupBy += ', ia.item_id';
        } else {
            select += ', d.item_id, d.page_id, d.activity_id, d.lesson_id ';
            from +=
                'left outer join ( \
                         select ia.asset_id, ia.item_id, p.id page_id, a2.id activity_id, l.id lesson_id \
                         from item_assets ia \
                             left outer join items i on i.id = ia.item_id \
                             left outer join pages p on p.id = i.page_id \
                             left outer join activities a2 on a2.id = p.activity_id \
                             left outer join lessons l on l.id = a2.lesson_id \
                         where l.site_id = ? \
                     ) d on d.asset_id = a.id \
                     left join asset_sites sa2 on a.id = sa2.asset_id ';
            params.unshift(siteId);
            groupBy += ', d.item_id';
        }
    }

    const rows = await query(select + from + where + groupBy, params);

    if (rows.length > 0) {
        const asset = rows[0];
        asset.metadata = JSON.parse(asset.metadata);

        if (detail) {
            asset.usage = [];
            const sitesTemp = JSON.parse('[' + asset.sites + ']');
            const done = {},
                sites = [];
            let isOwner = false;

            for (let i = 0; i < sitesTemp.length; i++) {
                if (done[sitesTemp[i].site_id] === undefined) {
                    done[sitesTemp[i].site_id] = true;
                    sites.push(sitesTemp[i]);
                    isOwner = sitesTemp[i].site_id === siteId && sitesTemp[i].owner;
                }
            }

            asset.sites = sites;
            asset.is_owner = isOwner;

            for (const k in rows) {
                const row = rows[k];
                if (!row.item_id) {
                    continue;
                }

                const usage = {
                    item_id: row.item_id,
                    page_id: row.page_id,
                    activity_id: row.activity_id,
                    lesson_id: row.lesson_id,
                };

                if (row.site_id) {
                    usage['site_id'] = row.site_id;
                }

                asset.usage.push(usage);
            }
        }
        delete asset.item_id;
        delete asset.page_id;
        delete asset.activity_id;
        delete asset.lesson_id;
        delete asset.site_id;
        return asset;
    } else {
        return null;
    }
}

export async function credits(siteId) {
    const sql = `select distinct a.* 
                    from assets a 
                        inner join asset_sites asi on a.id = asi.asset_id 
                        inner join item_assets ia on a.id = ia.asset_id 
                        inner join items i on ia.item_id = i.id 
                        inner join pages p on i.page_id = p.id 
                        inner join activities a2 on p.activity_id = a2.id 
                        inner join lessons l on a2.lesson_id = l.id 
                    where a.type='d' and asi.site_id = ? and l.state = ? 
                        and ( 
                            (a.credits_source is not null and a.credits_source <> '') 
                                or (a.credits_person is not null and a.credits_person <> '') 
                                or (a.license is not null and a.license <> '') 
                            ) 
                    order by a.caption`;

    const rows = await query(sql, [siteId, STATE_PUBLISHED]);
    const data = [];

    for (let i = 0; i < rows.length; i++) {
        const asset = rows[i];

        if (asset.metadata) {
            asset.metadata = JSON.parse(asset.metadata);
        }

        data.push(asset);
    }

    return data;
}

export async function listItems(siteId, type, usage) {
    let select = `select distinct a.*, group_concat(json_object("site_id", sa.site_id, "owner", sa.owner mod 2)) as sites`,
        from = ` from assets a 
                inner join asset_sites s on a.id = s.asset_id
                inner join asset_sites sa on a.id = sa.asset_id`,
        where = ' where s.site_id = ?';
    const params = [siteId],
        groupBy = ' group by a.id';
    if (type) {
        where += ' and a.type = ?';
        params.push(type);
    }
    if (usage) {
        select += ', d.qty usages';
        from +=
            ' left outer join (select ia.asset_id, count(ia.asset_id) qty \
                    from item_assets ia \
                        left outer join items i on i.id = ia.item_id \
                        left outer join pages p on p.id = i.page_id \
                        left outer join activities a2 on a2.id = p.activity_id \
                        left outer join lessons l on l.id = a2.lesson_id \
                    where l.site_id = ? \
                    group by ia.asset_id \
                ) d on d.asset_id = a.id';
        // needs to be pre-pended because it is before the where clause.
        params.unshift(siteId);
    }

    const results = await query(select + from + where + groupBy + ' order by a.caption', params);

    const data = [];
    for (const k in results) {
        const row = results[k];

        if (row.metadata) {
            row.metadata = JSON.parse(row.metadata);
        }
        row.sites = JSON.parse('[' + row.sites + ']');

        let isOwner = false;

        for (let i = 0; i < row.sites.length; i++) {
            const site = row.sites[i];

            if (site.site_id === siteId && site.owner) {
                isOwner = true;
                break;
            }
        }

        row.is_owner = isOwner;
        data.push(row);
    }
    return data;
}

export async function getList(ids) {
    const isQs = new Array(ids.length).fill('?', 0, ids.length).join(',');
    const rows = await query(`select * from assets where id in (${isQs})`, ids);

    const assets = [];

    for (let i = 0; i < rows.length; i++) {
        const a = rows[i];

        if (a.metadata) {
            a.metadata = JSON.parse(a.metadata);
        }
        assets.push(a);
    }

    return assets;
}

export async function search(siteId, type, name) {
    const rows = await query(
        'select assets.* from assets join asset_sites s on assets.id = s.asset_id where type = ? and s.site_id = ? and caption like ?',
        [type, siteId, `%${name}%`]
    );

    const data = [];
    for (const k in rows) {
        const row = rows[k];

        if (row.metadata) {
            row.metadata = JSON.parse(row.metadata);
        }
        data.push(row);
    }
    return data;
}

/**
 *
 * @param siteId
 * @param asset
 * @return {Promise<boolean>} true when it was only deleted for the specified site, false when it was deleted for all sites.
 */
export async function deleteAsset(siteId, asset) {
    const fullAsset = await getAsset(false, asset.id, true);

    if (fullAsset.usage.length > 0) {
        // only delete for current site.
        await query('delete from asset_sites where asset_id = ? and site_id = ?', [asset.id, siteId]);
        return true;
    } else {
        await query('delete from assets where id = ?', [asset.id]);

        return false;
    }
}

export async function saveAsset(siteId, asset) {
    if (asset.id) {
        const args = [
            asset.name,
            asset.caption,
            asset.credits_source,
            asset.credits_person,
            asset.license,
            asset.type,
            asset.subtype,
            JSON.stringify(asset.metadata),
            asset.id,
        ];
        await query(
            'update assets set name = ?, caption = ?, credits_source = ?, credits_person = ?, license = ?, type = ?, subtype = ?, metadata = ? where id = ?',
            args
        );
        return await getAsset(siteId, asset.id);
    } else {
        const args = [
            asset.name,
            asset.type,
            asset.subtype,
            asset.caption,
            asset.credits_source,
            asset.credits_person,
            asset.license.length > 0 ? asset.license : null,
            JSON.stringify(asset.metadata),
        ];

        const result = await query(
            'insert into assets (name, type, subtype, caption, credits_source, credits_person, license, metadata) values (?, ?, ?, ?, ?, ?, ?, ?)',
            args
        );
        await query('insert into asset_sites (asset_id, site_id, owner) VALUES (?, ?, ?)', [
            result.insertId,
            siteId,
            true,
        ]);

        return getAsset(siteId, result.insertId);
    }
}

/**
 *
 * @param siteId String
 * @param assetId Number Integer asset id to update
 * @param sites Array Array of site IDs to apply to the asset.
 * @param addOnly Bool If the new site should be added only, not previous sites removed.
 * @return {Promise<any>}
 */
export async function saveSites(siteId, assetId, sites, addOnly) {
    const doSave = async function () {
        const Qs = new Array(sites.length).fill('(?, ?, ?)').join(','),
            // Insert Ignore so that we don't have to worry about pre-existing permissions.
            sql = `insert ignore into asset_sites (asset_id, site_id, owner) values ${Qs}`,
            params = [];
        for (let i = 0; i < sites.length; i++) {
            params.push(assetId);
            params.push(sites[i]);
            params.push(0);
        }

        await query(sql, params);
        return getAsset(siteId, assetId, true);
    };

    if (addOnly === true) {
        return doSave();
    }
    const asset = await getAsset(false, assetId, true);
    const params = [];

    for (let i = 0; i < asset.usage.length; i++) {
        const u = asset.usage[i];

        if (params.indexOf(u.site_id) < 0) {
            params.push(u.site_id);
        }
    }

    const inQs = new Array(params.length).fill('?').join(',');
    let sql;
    if (params.length > 0) {
        sql = `delete from asset_sites where site_id not in (${inQs}) and asset_id = ? and owner = false`;
    } else {
        // Not used anywhere.
        sql = `delete from asset_sites where asset_id = ? and owner = false`;
    }

    params.push(assetId);

    // Delete all site records where it isn't used.
    await query(sql, params);

    return doSave();
}
