import db from './main';
import { query } from './database';

async function getContactId(siteId) {
  const sql = 'select id from contact where site_id = ?';

  const result = await query(sql, [siteId]);

  if (result.length === 0) {
    const sql = 'insert into contact (site_id, content_page_id, enabled) values (?, ?, 0)';

    const insertRes = await query(sql, [siteId, null]);

    return insertRes.insertId;
  }
  return result[0].id;
}

export function saveRecipient(siteId, recipient) {
  return new Promise((resolve, reject) => {
    if (recipient.id) {
      const sql = 'update contact_recipients set recipient = ? where id = ?';

      db.query(sql, [recipient.recipient, recipient.id], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(recipient.id);
        }
      });
    } else {
      getContactId(siteId)
        .then((contactId) => {
          getNextOrder(contactId)
            .then((nextOrder) => {
              const sql = 'insert into contact_recipients (contact_id, `order`) values (?, ?)';

              db.query(sql, [contactId, nextOrder], (err, result) => {
                if (err) {
                  reject(err);
                } else {
                  resolve(result.insertId);
                }
              });
            })
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    }
  });
}

export async function getContactPage(siteId) {
  const sql =
    'select c.content_page_id, cr.id \
                    from contact c \
                        join contact_recipients cr on c.id = cr.contact_id \
                    where site_id = ? and enabled = 1 \
                    order by cr.`order`';

  const results = await query(sql, [siteId]);
  if (results.length === 0) {
    return false;
  }
  const data = {
    content_page_id: results[0].content_page_id,
    recipients: [],
  };

  for (let i = 0; i < results.length; i++) {
    const row = results[i];

    data.recipients.push({
      id: row.id,
    });
  }

  return data;
}

export function getContact(siteId) {
  return new Promise((resolve, reject) => {
    const sql =
      'select c.content_page_id, c.enabled, cr.id id, cr.`order`, cr.recipient \
                    from contact c \
                        join contact_recipients cr on c.id = cr.contact_id \
                    where c.site_id = ? \
                    order by cr.`order`';
    db.query(sql, [siteId], (err, results) => {
      if (err) {
        reject(err);
      } else {
        if (results.length === 0) {
          resolve({});
          return;
        }
        const data = {
          content_page_id: results[0].content_page_id,
          enabled: results[0].enabled,
          recipients: [],
        };

        for (let i = 0; i < results.length; i++) {
          const row = results[i];
          data.recipients.push({
            id: row.id,
            recipient: row.recipient,
            order: row.order,
          });
        }

        resolve(data);
      }
    });
  });
}

export function saveContact(siteId, contact) {
  return new Promise((resolve, reject) => {
    getContactId(siteId).then((contactId) => {
      const sql = 'update contact set content_page_id = ?, enabled = ? where id = ?';

      let pageId = null,
        enabled = 0;
      if (contact.content_page_id) {
        pageId = contact.content_page_id;
      }
      if (contact.enabled) {
        enabled = 1;
      }

      db.query(sql, [pageId, enabled, contactId], (err) => {
        if (err) {
          reject(err);
        } else {
          if (contact.recipients && contact.recipients.length > 0) {
            const queries = [];

            for (let i = 0; i < contact.recipients.length; i++) {
              queries.push(saveRecipient(siteId, contact.recipients[i]));
            }

            Promise.all(queries)
              .then(() => {
                resolve(contact.id);
              })
              .catch((err) => reject(err));
          }
        }
      });
    });
  });
}

export function getNextOrder(contactId) {
  return new Promise((resolve, reject) => {
    const sql =
      'select coalesce(max(`order`), 0) + 1 as next_order from contact_recipients where contact_id = ?';

    db.query(sql, [contactId], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result[0].next_order);
      }
    });
  });
}

export async function getRecipient(siteId, recipientId) {
  const contactId = await getContactId(siteId),
    sql = 'select cr.recipient from contact_recipients cr where contact_id = ? and id = ?',
    results = await query(sql, [contactId, recipientId]);

  return results[0];
}

export function deleteRecipient(siteId, recipientId) {
  return new Promise((resolve, reject) => {
    getContactId(siteId)
      .then((contactId) => {
        const sql = 'delete from contact_recipients where contact_id = ? and id = ?';

        db.query(sql, [contactId, recipientId], (err) => {
          if (err) {
            reject(err);
          } else {
            resolve(true);
          }
        });
      })
      .catch((err) => reject(err));
  });
}

export async function saveOrder(siteId, order) {
  const contactId = await getContactId(siteId);

  const sql = 'update contact_recipients set `order` = ? where contact_id = ? and id = ?';

  const queries = [];
  for (let i = 0; i < order.length; i++) {
    queries.push(query(sql, [i, contactId, order[i]]));
  }

  await Promise.all(queries);

  return queries.length;
}
