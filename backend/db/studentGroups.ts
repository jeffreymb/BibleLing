import { InsertResult, query } from './database';

interface Group {
  id: number;
  name: string;
  inactive: boolean;
}

interface GroupDetail extends Group {
  qty_students: number;
}

export async function listGroups(
  siteId: string,
  includeInactive = false
): Promise<Array<GroupDetail>> {
  let where = '';
  if (!includeInactive) {
    where = 'and s.inactive = 0';
  }
  return query<Array<GroupDetail>>(
    `select s.id, s.name, s.inactive, count(sgs.user_id) qty_students 
  from student_groups s \
    left join student_groups_students sgs on s.id = sgs.group_id \
  where s.site_id = ? ${where} \
  group by s.id, s.name, s.inactive \
  order by s.name`,
    [siteId]
  );
}

export async function loadGroup(siteId: string, groupId: number): Promise<Group | null> {
  const result = await query<Array<Group>>(
    'select id, name, inactive \
  from student_groups\
  where id = ? and site_id = ?',
    [groupId, siteId]
  );

  if (result.length === 0) {
    return null;
  }
  return result[0];
}

export async function createGroup(siteId: string, group: Omit<Group, 'id'>): Promise<Group> {
  const result = await query<InsertResult>(
    'insert into student_groups (site_id, name, inactive) \
  values (?, ?, ?)',
    [siteId, group.name, group.inactive]
  );

  return (await loadGroup(siteId, result.insertId)) as Group;
}

export async function deleteGroup(siteId: string, groupId: number): Promise<void> {
  await query('delete from student_groups where site_id = ? and id = ?', [siteId, groupId]);
}

export async function addStudentsToGroup(
  siteId: string,
  groupId: number,
  userIds: Array<string>
): Promise<void> {
  const group = await loadGroup(siteId, groupId);

  if (!group) {
    // Group not found or incorrect site ID.
    return;
  }

  const values: Array<string | number> = [],
    parts: Array<string> = [];

  userIds.map((uid) => {
    values.push(groupId, uid);
    parts.push('(?,?)');
  });

  await query<InsertResult>(
    'insert ignore into student_groups_students \
  (group_id, user_id) \
  values ' +
      parts.join(','),
    values
  );
}

export async function removeStudentFromGroup(
  siteId: string,
  groupId: number,
  userId: string
): Promise<void> {
  const group = await loadGroup(siteId, groupId);

  if (!group) {
    return;
  }

  await query('delete from student_groups_students \
  where group_id = ? and user_id = ?', [
    groupId,
    userId,
  ]);
}

export async function updateGroup(siteId: string, group: Group): Promise<Group> {
  const result = await query(
    'update student_groups \
  set name = ?, inactive = ? \
  where id = ? and site_id = ?',
    [group.name, group.inactive, group.id, siteId]
  );

  console.log(result);

  return (await loadGroup(siteId, group.id)) as Group;
}

export async function listStudents(
  siteId: string,
  groupId: number
): Promise<Array<{ user_id: string }>> {
  return await query<Array<{ user_id: string }>>(
    'select s.user_id \
  from student_groups_students s \
    join student_groups g \
  where s.group_id = ? and g.site_id = ?',
    [groupId, siteId]
  );
}
