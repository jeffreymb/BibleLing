import db from './database';
import { saveItems } from './item';
import { getContent } from './content';
import _ from 'lodash';

async function saveAssets(pageId, assets) {
    if (assets.length === 0) {
        return;
    }
    await db.query('delete from page_assets where page_id = ?', [pageId]);

    const args = [];

    for (let i = 0; i < assets.length; i++) {
        args.push(pageId);
        args.push(assets[i].id);
    }
    if (args.length === 0) {
        return;
    }

    const q = new Array(args.length / 2).fill('(?,?)').join(',');

    await db.query('insert into page_assets (page_id, asset_id) values ' + q, args);
}

export async function getPages(activityId) {
    const rows = await db.query(
        'select p.id, p.note, p.config, p.content_id, \
                        i.id i_id, i.order i_order, i.config i_config, i.content_id i_content_id, \
                        ia.asset_id, a.name a_name, a.caption a_caption, a.type a_type, a.subtype a_subtype, a.credits_person, a.credits_source, a.license, a.metadata a_metadata \
                    from pages p \
                    left join items i on i.page_id = p.id \
                    left join item_assets ia on i.id = ia.item_id \
                    left join assets a on a.id = ia.asset_id \
                    where activity_id = ? \
                    order by p.`order`, i.`order`, ia.`order`',
        [activityId]
    );
    const data = [],
        map = {},
        itemMap = {},
        contentIds = [];

    for (let key = 0; key < rows.length; key++) {
        const row = rows[key];

        if (map[row.id] === undefined) {
            data.push({
                id: row.id,
                note: row.note,
                content: {
                    id: row.content_id,
                    label: null,
                    text_full: null,
                    text: [],
                },
                config: JSON.parse(row.config),
                items: [],
                assets: [],
            });
            if (row.content_id) {
                contentIds.push(row.content_id);
            }

            map[row.id] = data.length - 1;
            itemMap[row.id] = {};
        }

        if (row.i_id !== null) {
            let page = data[map[row.id]];
            if (itemMap[row.id][row.i_id] === undefined) {
                // this item isn't saved yet.
                page.items.push({
                    id: row.i_id,
                    config: row.i_config ? JSON.parse(row.i_config) : {},
                    content: {
                        id: row.i_content_id,
                        label: null,
                        text_full: null,
                        text: [],
                    },
                    assets: [],
                });

                if (row.i_content_id) {
                    contentIds.push(row.i_content_id);
                }

                itemMap[row.id][row.i_id] = page.items.length - 1;
            }

            if (row.asset_id !== null) {
                page.items[itemMap[row.id][row.i_id]].assets.push({
                    id: row.asset_id,
                    name: row.a_name,
                    caption: row.a_caption,
                    order: row.a_order,
                    type: row.a_type,
                    subtype: row.a_subtype,
                    metadata: JSON.parse(row.a_metadata),
                    // Need these fields primarily for mobile as it downloads and stores
                    // the asset info from the activity.
                    credits_source: row.credits_source,
                    credits_person: row.credits_person,
                    license: row.license,
                });
            }
        }
    }

    if (contentIds.length > 0) {
        const content = await getContent(contentIds);
        for (let p = 0; p < data.length; p++) {
            let page = data[p];

            if (page.content.id) {
                const c = _.find(content, (c) => c.id === page.content.id);
                page.content.text = c.text;
                page.content.text_full = c.text_full;
            }

            for (let i = 0; i < page.items.length; i++) {
                const item = page.items[i];

                if (item.content.id) {
                    const c = _.find(content, (c) => c.id === item.content.id);
                    item.content.text = c.text;
                    item.content.text_full = c.text_full;
                }
            }
        }
    }

    if (data.length > 0) {
        // Populate page assets.
        const pageIdsQs = new Array(data.length).fill('?').join(',');
        const assets = await db.query(
            'select pi.page_id, \
                a.id, a.name, a.caption, a.type, a.subtype, a.credits_person, a.credits_source, a.license, a.metadata \
            from page_assets pi \
                join assets a on a.id = pi.asset_id \
            where pi.page_id in (' +
                pageIdsQs +
                ')',
            Object.keys(map)
        );

        for (let i = 0; i < assets.length; i++) {
            const asset = assets[i],
                pageId = asset.page_id;
            asset.metadata = JSON.parse(asset.metadata);
            delete asset.page_id;

            data[map[pageId]].assets.push(asset);
        }
    }

    return data;
}

export async function updatePages(activityId, pages) {
    if (!pages) {
        pages = [];
    }
    // we don't want to short-circuit on no pages passed in case there were some deleted.
    let newPages = [],
        updatedPages = [],
        newArgs = [],
        deleteArgs = [];
    for (let i = 0; i < pages.length; i++) {
        let page = pages[i];

        page.order = i;

        if (!page.id) {
            if (!page.config) {
                page.config = {};
            }
            newPages.push(page);
            newArgs.push(activityId);
            newArgs.push(page.order);
            newArgs.push(page.note);
            newArgs.push(JSON.stringify(page.config));
            if (page.content && page.content.id) {
                newArgs.push(page.content.id);
            } else {
                newArgs.push(null);
            }
        } else {
            deleteArgs.push(page.id);
            updatedPages.push(page);
        }
    }
    let newQ = new Array(newPages.length).fill('(?,?,?,?,?)').join(','),
        insertSql = 'insert into pages (activity_id, `order`, note, config, content_id) values ' + newQ,
        updateSql = 'update pages set `order` = ?, note=?, config=?, content_id=? where id = ?',
        deleteQ = '(' + new Array(deleteArgs.length).fill('?').join(',') + ')',
        deleteSql = 'delete from pages where activity_id = ?';
    if (deleteArgs.length > 0) {
        deleteSql += ' and id not in ' + deleteQ;
    }
    deleteArgs.unshift(activityId);

    await db.query(deleteSql, deleteArgs);

    const pending = [];
    if (newArgs.length > 0) {
        pending.push(
            (async function () {
                await db.query(insertSql, newArgs);

                const orders = _.map(newPages, (p) => p.order),
                    orderQs = new Array(orders.length).fill('?').join(','),
                    orderSql = 'select id, `order` from pages where activity_id = ? and `order` in (' + orderQs + ')';
                orders.unshift(activityId);
                const rows = await db.query(orderSql, orders);
                const pendingSaves = [];
                for (let i = 0; i < rows.length; i++) {
                    const r = rows[i];
                    const page = _.find(newPages, (p) => p.order === r.order);
                    page.id = r.id;
                    pendingSaves.push(saveItems(page.id, page.items));
                    pendingSaves.push(saveAssets(page.id, page.assets));
                }
                await Promise.all(pendingSaves);
            })()
        );
    }

    if (updatedPages.length > 0) {
        for (const p in updatedPages) {
            const page = updatedPages[p];
            pending.push(
                (async function () {
                    let contentId = null;
                    if (page.content && page.content.id) {
                        contentId = page.content.id;
                    }
                    await db.query(updateSql, [page.order, page.note, JSON.stringify(page.config), contentId, page.id]);
                    await saveItems(page.id, page.items);
                    await saveAssets(page.id, page.assets);
                })()
            );
        }
    }

    await Promise.all(pending);
    return await getPages(activityId);
}
