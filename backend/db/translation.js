import { query } from './database';

export async function languageExists(sitId, lang) {
  const rows = await query('SELECT lang from languages WHERE lang=? and site_id=?', [lang, sitId]);
  return rows.length > 0;
}

export async function saveLanguage(siteId, lang) {
  const exists = await languageExists(siteId, lang);

  if (exists) {
    return true;
  }

  await query('insert into languages (lang, site_id) values(?, ?)', [lang, siteId]);

  return true;
}

export async function getLanguages(siteId) {
  const rows = await query('select * from languages where site_id = ?', [siteId]);

  const list = [];
  for (let i = 0; i < rows.length; i++) {
    list.push(rows[i].lang);
  }

  return list;
}

/**
 * @param siteId string
 * @param lang
 * @param ids (optional)
 * @return {Promise<Array>}
 */
export async function getTranslations(siteId, lang, ids) {
  // make a string with the correct number of question marks for the number of ids.
  let q = '',
    args = [],
    sql = 'select string_id, `text` from translations where lang=? and site_id=?';
  if (ids && ids.length > 0) {
    q = new Array(ids.length).fill('?').join(',');
    args = ids;
    sql += ' and string_id IN (' + q + ')';
  }
  sql += ' order by string_id';
  // prepend the site and language id
  args.unshift(siteId);
  args.unshift(lang);

  const rows = await query(sql, args);
  const translations = {};

  for (let i = 0; i < rows.length; i++) {
    translations[rows[i].string_id] = rows[i].text;
  }

  return translations;
}

export async function getLanguagesForString(siteId, stringId) {
  const rows = await query(
    'select lang, text from translations where string_id = ? and site_id = ?',
    [stringId, siteId]
  );

  const translations = {};

  for (let i = 0; i < rows.length; i++) {
    translations[rows[i].lang] = rows[i].text;
  }

  return translations;
}

export async function saveTranslation(siteId, stringId, translation) {
  const keys = Object.keys(translation),
    q = new Array(keys.length).fill('(?,?,?,?)').join(','),
    sql = 'replace into translations (lang, site_id, string_id, text) values ' + q,
    args = [];
  if (keys.length === 0) {
    return true;
  }

  for (const k in keys) {
    const key = keys[k];

    args.push(key);
    args.push(siteId);
    args.push(stringId);
    args.push(translation[key]);
  }

  await query(sql, args);

  return true;
}

export async function saveTranslationForLang(siteId, lang, translations) {
  const keys = Object.keys(translations),
    replaceArgs = [],
    deleteArgs = [];
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (translations[key]) {
      replaceArgs.push(lang);
      replaceArgs.push(siteId);
      replaceArgs.push(key);
      replaceArgs.push(translations[key]);
    } else {
      deleteArgs.push(key);
    }
  }

  if (replaceArgs.length > 0) {
    const q = new Array(replaceArgs.length / 4).fill('(?,?,?,?)').join(','),
      sql = 'replace into translations (lang, site_id, string_id, text) values ' + q;

    await query(sql, replaceArgs);
  }

  if (deleteArgs.length > 0) {
    await query('delete from translations where site_id = ? and lang = ? and string_id in (?)', [
      siteId,
      lang,
      deleteArgs,
    ]);
  }

  return true;
}

export async function deleteTranslation(siteId, stringId) {
  const sql = 'delete from translations where site_id = ? and string_id = ?';

  await query(sql, [siteId, stringId]);

  return true;
}

export async function copyBetweenSites(toSiteId, fromSiteId, lang) {
  const targetLang = await escape(lang);
  toSiteId = await escape(toSiteId);

  const sql = `insert ignore into translations (lang, site_id, string_id, text)
        select ${targetLang}, ${toSiteId}, t.string_id, t.text 
        from translations t 
        where t.lang = ? and t.site_id = ? and t.string_id not like '\\_%'`;

  await query(sql, [lang, fromSiteId]);
  return true;
}
