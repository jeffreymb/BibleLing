import dbAsync from './database';
import database from './database';
import db from './main';

interface ContentPage {
  id: number;
  public: boolean;
  languages: Array<unknown>;
}

export const STATE_DRAFT = 0;
export const STATE_PUBLSISHED = 1;

export function listAll(siteId: string): Promise<Array<ContentPage>> {
  return new Promise((resolve, reject) => {
    const sql =
      'select cp.id, cp.public, cpt.lang, cpt.state \
                    from content_pages cp \
                        left join content_page_translations cpt on cp.id = cpt.id and cp.site_id = cpt.site_id \
                    where cp.site_id = ? \
                    order by cp.`order`, cpt.lang';
    db.query(sql, [siteId], (err, result) => {
      if (err) {
        reject(err);
      } else {
        const rows: Array<ContentPage> = [];

        for (let i = 0; i < result.length; i++) {
          const r = result[i],
            item = {
              lang: r.lang,
              state: r.state,
            },
            hasPrev = rows.length > 0 && rows[rows.length - 1].id === r.id;
          if (hasPrev) {
            rows[rows.length - 1].languages.push(item);
          } else {
            const langs = [];
            if (item.lang && item.state !== null) {
              langs.push(item);
            }
            rows.push({
              id: r.id,
              public: r.public === 1,
              languages: langs,
            });
          }
        }

        resolve(rows);
      }
    });
  });
}

export async function listForLang(
  siteId: string,
  lang: string,
  state: boolean,
  isPublic: boolean
): Promise<Array<Record<string, unknown>>> {
  const sql =
    'select cp.id, cpt.state \
                    from content_pages cp \
                        join content_page_translations cpt on cp.id = cpt.id and cp.site_id = cpt.site_id \
                    where cp.site_id = ? and cpt.lang = ? and cpt.state >= ? and cp.public = ? \
                    order by cp.`order`';

  return (dbAsync.query(sql, [siteId, lang, state, isPublic]) as unknown) as Array<
    Record<string, unknown>
  >;
}

export function getForLang(
  siteId: string,
  id: number,
  lang: string,
  state: boolean
): Promise<void> {
  return new Promise((resolve, reject) => {
    const sql =
      'select cpt.id, cpt.state, cpt.lang, cpt.text \
                    from content_page_translations cpt \
                    where cpt.site_id = ? and cpt.id = ? and cpt.lang = ? and cpt.state >= ? \
                    limit 1';

    db.query(sql, [siteId, id, lang, state], (err, results) => {
      if (err) {
        reject(err);
      } else {
        if (results.length === 1) {
          resolve(results[0]);
        } else {
          resolve();
        }
      }
    });
  });
}

export function create(siteId: string, id: number): Promise<number> {
  return new Promise((resolve, reject) => {
    db.query(
      'select coalesce(max(`order`) + 1, 0) next_order from content_pages where site_id = ?',
      [siteId],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          const sql =
              'insert into content_pages (id, site_id, `order`, public, config) values (?, ?, ?, ?, ?)',
            nextOrder = result[0].next_order;
          db.query(sql, [id, siteId, nextOrder, false, '{}'], (err) => {
            if (err) {
              reject(err);
            } else {
              resolve(id);
            }
          });
        }
      }
    );
  });
}

export function savePage(siteId: string, id: number, page: { public: boolean }) {
  return new Promise((resolve, reject) => {
    const sql = 'update content_pages set public = ? where site_id = ? and id = ?';

    db.query(sql, [page.public, siteId, id], (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

interface Content {
  state: boolean;
  text: string;
}

export async function saveContent(
  siteId: string,
  id: number,
  lang: string,
  content: Content
): Promise<boolean> {
  const sql =
    'replace into content_page_translations (id, site_id, lang, state, text) \
                     values (?, ?, ?, ?, ?)';

  await dbAsync.query(sql, [id, siteId, lang, content.state, content.text]);

  return true;
}

export async function saveOrder(siteId: string, order: Array<number>): Promise<number> {
  const sql = 'update content_pages set `order` = ? where site_id = ? and id = ?',
    queries = [];

  for (let i = 0; i < order.length; i++) {
    queries.push(dbAsync.query(sql, [i, siteId, order[i]]));
  }
  await Promise.all(queries);

  return queries.length;
}

export async function deletePage(siteId: string, id: number): Promise<boolean> {
  const sql = 'delete from content_pages where site_id = ? and id = ?';
  await database.query(sql, [siteId, id]);
  return true;
}
