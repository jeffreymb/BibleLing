import db from './main';
import database from './database';

export async function searchContent(siteId, contentSearch, labelSearch) {
    if (!contentSearch && !labelSearch) {
        return [];
    }
    let sql = 'select id from content where site_id = ? AND ',
        searchParts = [],
        queryArgs = [siteId];
    if (contentSearch) {
        searchParts.push('text_full like ?');
        queryArgs.push('%' + contentSearch + '%');
    }

    if (labelSearch) {
        searchParts.push('label like ?');
        queryArgs.push('%' + labelSearch + '%');
    }

    sql += searchParts.join(' AND ') + ' LIMIT 25';

    const ids = await database.query(sql, queryArgs);

    return await getContent(ids.map((row) => row.id));
}

/**
 *
 * @param id Array|int
 * @return {Promise}
 */
export function getContent(id) {
    return new Promise((resolve, reject) => {
        let where = '',
            args = [],
            multiple = false;
        if (Array.isArray(id)) {
            if (id.length === 0) {
                resolve([]);
                return;
            }
            multiple = true;
            where = 'c.id in (' + new Array(id.length).fill('?').join(',') + ')';
            args = id;
        } else {
            where = 'c.id = ?';
            args.push(id);
        }

        const sql = `select c.id, c.label, c.text_full, ct.content_id, ct.form_id, ct.text \
                    from content c \
                        left join content_text ct on c.id = ct.content_id \
                    where ${where} order by ct.\`order\``;

        db.query(sql, args, (err, results) => {
            if (err) {
                reject(err);
            } else {
                let content = [],
                    idMap = {};

                for (let i = 0; i < results.length; i++) {
                    const row = results[i];

                    if (idMap[row.id] === undefined) {
                        idMap[row.id] = content.length;

                        content.push({
                            id: row.id,
                            label: row.label,
                            text_full: row.text_full,
                            text: [],
                        });
                    }

                    if (row.content_id) {
                        content[idMap[row.id]].text.push({
                            text: row.text,
                            form_id: row.form_id,
                        });
                    }
                }

                if (multiple) {
                    resolve(content);
                } else if (content.length === 1) {
                    resolve(content[0]);
                } else {
                    resolve({});
                }
            }
        });
    });
}

export function saveContent(siteId, content) {
    const save = new Promise((resolve, reject) => {
        if (!content.id) {
            const sql = 'insert into content (site_id, label, text_full) values (?, ?, ?)';

            db.query(sql, [siteId, content.label, content.text_full], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    content.id = result.insertId;

                    resolve(content);
                }
            });
        } else {
            const sql = 'update content set label=?, text_full=? where id = ? and site_id = ?';
            db.query(sql, [content.label, content.text_full, content.id, siteId], (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(content);
                }
            });
        }
    });

    return new Promise((resolve, reject) => {
        save.then((content) => {
            db.query('delete from content_text where content_id = ?', [content.id], (err) => {
                if (err) {
                    reject(err);
                    return;
                } else if (content.text.length === 0) {
                    getContent(content.id)
                        .then((data) => resolve(data))
                        .catch((err) => reject(err));
                    return;
                }

                let insertArgs = [],
                    q = new Array(content.text.length).fill('(?,?,?,?)').join(','),
                    sql = 'insert into content_text (content_id, `order`, text, form_id) values ' + q;
                for (let i = 0; i < content.text.length; i++) {
                    const text = content.text[i];

                    insertArgs.push(content.id);
                    insertArgs.push(i); // order
                    insertArgs.push(text.text);
                    insertArgs.push(text.form_id);
                }

                db.query(sql, insertArgs, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        getContent(content.id)
                            .then((data) => resolve(data))
                            .catch((err) => reject(err));
                    }
                });
            });
        }).catch((err) => reject(err));
    });
}
