import db from './main';
import { query } from './database';
import { getList } from './asset';
import _find from 'lodash/find';
import { deleteTranslation } from './translation';
import rollbar from '../config/rollbar';

export function listAll(siteId) {
  return new Promise((resolve, reject) => {
    db.query(
      'select lc.id, lc.`order`, lc.asset_id from lesson_categories lc where site_id=? order by `order`',
      [siteId],
      (err, rows) => {
        if (err) {
          reject(err);
        } else {
          const cats = [],
            assetIds = {};

          for (let i = 0; i < rows.length; i++) {
            const r = rows[i];

            r.asset = {};
            cats.push(r);
            if (r.asset_id !== null) {
              if (assetIds[r.asset_id] === undefined) {
                assetIds[r.asset_id] = [];
              }

              assetIds[r.asset_id].push(r.id);
            }
            delete r.asset_id;
          }

          const assetIdArray = Object.keys(assetIds);

          if (assetIdArray.length > 0) {
            getList(assetIdArray)
              .then((assets) => {
                for (let assetKey = 0; assetKey < assets.length; assetKey++) {
                  const a = assets[assetKey];

                  for (let k = 0; k < assetIds[a.id].length; k++) {
                    const category = _find(cats, (c) => c.id === assetIds[a.id][k]);
                    category.asset = a;
                  }
                }

                resolve(cats);
              })
              .catch((err) => reject(err));
          } else {
            resolve(cats);
          }
        }
      }
    );
  });
}

export function saveLessonCategory(siteId, category) {
  return new Promise((resolve, reject) => {
    if (category.id) {
      const sql = 'update lesson_categories set asset_id = ? where id = ? and site_id = ?',
        assetId = category.asset !== undefined ? category.asset.id : null;
      db.query(sql, [assetId, category.id, siteId], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    } else {
      db.query(
        'select coalesce(max(`order`), 0) + 1 new_order from lesson_categories where site_id = ?',
        [siteId],
        (err, rows) => {
          if (err) {
            reject(err);
            return;
          }

          db.query(
            'insert into lesson_categories (site_id, `order`, asset_id) values (?, ?, ?)',
            [siteId, rows[0].new_order, category.asset_id],
            (err, result) => {
              if (err) {
                reject(err);
              } else {
                resolve(result.insertId);
              }
            }
          );
        }
      );
    }
  });
}

export async function deleteLessonCategory(siteId, categoryId) {
  const sql = 'delete from lesson_categories where id = ? and site_id = ?';

  try {
    await query(sql, [categoryId, siteId]);
    await deleteTranslation(siteId, `_lesson_category_${categoryId}`);
    await deleteTranslation(siteId, `_lesson_category_credits_${categoryId}`);
  } catch (e) {
    rollbar.error(e);
    return false;
  }
  return true;
}

export async function saveOrder(siteId, order) {
  const sql = 'update lesson_categories set `order` = ? where id = ? and site_id = ?',
    queryQueue = [];

  for (let i = 0; i < order.length; i++) {
    queryQueue.push(query(sql, [i, order[i], siteId]));
  }

  await Promise.all(queryQueue);
  return order.length;
}
