import db from './main';
import _find from 'lodash/find';

export function getForPage(pageId) {
  return new Promise((resolve, reject) => {
    db.query(
      'select i.id, i.order, i.content_id, i.config, \
                        c.label, c.text_full, \
                        ia.asset_id, a.name, a.caption, a.type, ia.`order` \
                    from items i \
                        left join content c on c.id = i.content_id \
                        left join item_assets ia on i.id = ia.item_id \
                        left join assets a on ia.asset_id = a.id \
                    where i.page_id = ? \
                    order by i.`order`, ia.`order`',
      [pageId],
      (err, rows) => {
        if (err) {
          reject(err);
        } else {
          let data = [],
            map = {};

          for (let i = 0; i < rows.length; i++) {
            const row = rows[i];

            if (map[row.id] === undefined) {
              map[row.id] = data.length;

              data.push({
                id: row.id,
                order: row.order,
                config: row.config,
                content: {
                  id: row.content_id,
                  text_full: row.text_full,
                  label: row.label,
                  text: [],
                },
                assets: [],
              });
            }

            if (row.asset_id !== null) {
              data[map[row.id]].assets.push({
                id: row.asset_id,
                name: row.name,
                caption: row.caption,
                type: row.type,
                order: row.order,
              });
            }
          }

          resolve(data);
        }
      }
    );
  });
}

function populateIdsFromOrder(pageId, items) {
  return new Promise((resolve, reject) => {
    db.query('select id, `order` from items where page_id = ?', [pageId], (err, rows) => {
      if (err) {
        reject(err);
      } else {
        for (let i = 0; i < rows.length; i++) {
          const r = rows[i],
            item = _find(items, (i) => i.order === r.order);
          if (item) {
            item.id = r.id;
          }
        }
        resolve(items);
      }
    });
  });
}

function saveAssets(pageId, items) {
  return new Promise((resolve, reject) => {
    db.query(
      'delete ia from item_assets ia join items i on ia.item_id = i.id where i.page_id = ?',
      [pageId],
      (err) => {
        if (err) {
          reject(err);
        } else {
          const args = [];

          for (let i = 0; i < items.length; i++) {
            for (let k = 0; k < items[i].assets.length; k++) {
              args.push(items[i].id);
              args.push(items[i].assets[k].id);
              args.push(k);
            }
          }
          if (args.length === 0) {
            resolve();
            return;
          }

          let q = new Array(args.length / 3).fill('(?,?,?)').join(',');

          db.query(
            'insert into item_assets (item_id, asset_id, `order`) values ' + q,
            args,
            (err) => {
              if (err) {
                reject(err);
              } else {
                resolve();
              }
            }
          );
        }
      }
    );
  });
}

export function saveItems(pageId, items) {
  return new Promise((resolve, reject) => {
    const newItems = [],
      updatedItems = [],
      newArgs = [],
      deleteNotIn = [];
    for (let i = 0; i < items.length; i++) {
      const item = items[i];

      item.order = i;
      if (!item.id) {
        newItems.push(item);
        newArgs.push(pageId);
        newArgs.push(item.order);
        newArgs.push(item.content.id);
        newArgs.push(JSON.stringify(item.config));
      } else {
        deleteNotIn.push(item.id);
        updatedItems.push(item);
      }
    }

    let newQ = new Array(newItems.length).fill('(?,?,?,?)').join(','),
      insertSql = 'insert into items (page_id, `order`, content_id, config) values ' + newQ,
      updateSql = 'update items set `order` = ?, content_id = ?, config = ? where id = ?',
      deleteQ = '(' + new Array(deleteNotIn.length).fill('?').join(',') + ')',
      deleteSql = 'delete from items where page_id = ?';
    if (deleteNotIn.length > 0) {
      deleteSql += ' and id not in ' + deleteQ;
    }
    deleteNotIn.unshift(pageId);

    const deletePromise = new Promise((delResolve, delReject) => {
      db.query(deleteSql, deleteNotIn, (err) => {
        if (err) {
          delReject(err);
        } else {
          delResolve();
        }
      });
    });

    deletePromise
      .then(() => {
        const pending = [];
        if (newArgs.length > 0) {
          pending.push(
            new Promise((insResolve, insReject) => {
              db.query(insertSql, newArgs, (err) => {
                if (err) {
                  insReject(err);
                } else {
                  // we need to populate the item id so that we can insert any item_assets
                  populateIdsFromOrder(pageId, items)
                    .then((data) => {
                      insResolve();
                    })
                    .catch((err) => insReject(err));
                }
              });
            })
          );
        }

        if (updatedItems.length > 0) {
          for (let i = 0; i < updatedItems.length; i++) {
            const item = updatedItems[i];

            const promise = new Promise((updateResolve, updatedReject) => {
              let contentId = null;
              if (item.content && item.content.id) {
                contentId = item.content.id;
              }
              db.query(
                updateSql,
                [item.order, contentId, JSON.stringify(item.config), item.id],
                (err) => {
                  if (err) {
                    updatedReject(err);
                  } else {
                    updateResolve();
                  }
                }
              );
            });

            promise.catch((err) => reject(err));

            pending.push(promise);
          }
        }

        Promise.all(pending)
          .then(() => {
            saveAssets(pageId, items)
              .then(() => resolve())
              .catch((err) => reject(err));
          })
          .catch((err) => reject(err));
      })
      .catch((err) => reject(err));
  });
}
