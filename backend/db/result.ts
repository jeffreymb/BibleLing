import { query } from './database';
import rollbar from '../config/rollbar';
import activityConfig from '../config/ActivityTypes';

interface ItemForResult {
  id: number;
  activity_id: number;
  content_id: number;
  assets: Array<number>;
}

export async function incrementSummary(
  siteId: string,
  userId: string,
  takeId: number,
  activityId: number
): Promise<void> {
  const result = await query<Array<{ take_id: number }>>(
    'select 0 from results_summary where site_id = ? and user_id = ? and take_id = ?',
    [siteId, userId, takeId]
  );
  const now = Date.now();

  if (result.length != 0) {
    await query(
      'update results_summary set items_complete = items_complete + 1, completed = ? \
    where site_id = ? and user_id = ? and take_id = ?',
      [now, siteId, userId, takeId]
    );
  } else {
    const info = await query<Array<{ qty_items: number; qty_pages: number; type: string }>>(
      'select count(i.id) qty_items, count(distinct p.id) qty_pages, a.type \
    from activities a \
      join pages p on a.id = p.activity_id\
      join items i on p.id = i.page_id \
      join lessons l on a.lesson_id = l.id \
    where p.activity_id = ? and l.site_id = ? \
    group by a.id, a.type',
      [activityId, siteId]
    );

    if (info.length === 0) {
      return;
    }

    let qty = info[0].qty_items;
    const type = activityConfig[info[0].type];

    if (type.pages.options?.content === true) {
      qty = info[0].qty_pages;
    }

    await query(
      'insert into results_summary \
    (site_id, user_id, take_id, started, completed, items_qty, items_complete) \
    values (?, ?, ?, ?, ?, ?, 1)',
      [siteId, userId, takeId, now, now, qty]
    );
  }
}

export async function getItemForResult(
  siteId: string,
  itemId: number
): Promise<ItemForResult | null> {
  const result = await query<
    Array<{ id: number; activity_id: number; content_id: number; asset_id: number }>
  >(
    'select i.id, i.content_id, p.activity_id, ia.asset_id \
    from items i \
    left join item_assets ia on ia.item_id = i.id \
    join pages p on i.page_id = p.id \
    join activities a on p.activity_id = a.id \
    join lessons l on a.lesson_id = l.id \
    where l.site_id = ? and  i.id = ? \
    order by ia.`order`',
    [siteId, itemId]
  );
  if (result.length === 0) {
    return null;
  }

  const item: ItemForResult = {
    id: result[0].id,
    activity_id: result[0].activity_id,
    content_id: result[0].content_id,
    assets: [],
  };

  for (let i = 0; i < result.length; i++) {
    if (result[i].asset_id) {
      item.assets.push(result[i].asset_id);
    }
  }
  return item;
}

export async function saveResult(
  siteId: string,
  userId: string,
  takeId: number,
  item: ItemForResult,
  isCorrect: boolean
): Promise<boolean> {
  try {
    await query(
      'insert into results (site_id, user_id, take_id, activity_id, item_id, is_correct, content_id, assets) \
      values (?, ?, ?, ?, ?, ?, ?, ?)',
      [
        siteId,
        userId,
        takeId,
        item.activity_id,
        item.id,
        isCorrect,
        item.content_id,
        JSON.stringify(item.assets),
      ]
    );
  } catch (e) {
    rollbar.error(e);
    return false;
  }
  return true;
}

export async function listUsers(
  siteId: string,
  page = 0,
  qtyPerPage = 50
): Promise<Array<{ user_id: string; started: number }>> {
  return query(
    'select s.user_id, max(s.started) started \
  from results_summary s \
  where s.site_id = ? \
  group by s.user_id \
  order by max(s.started) desc \
  limit ?, ?',
    [siteId, page * qtyPerPage, qtyPerPage]
  );
}

export async function countUsers(siteId: string): Promise<number> {
  const res = await query<Array<{ qty: number }>>(
    'select count(distinct s.user_id) qty from results_summary s where s.site_id = ?',
    [siteId]
  );

  if (res.length === 0) {
    return 0;
  }

  return res[0].qty;
}

export async function getResultsForUser(
  siteId: string,
  uid: string,
  page = 0,
  qtyPerpage = 50
): Promise<
  Array<{
    started: number;
    completed: number;
    complete: number;
    qty: number;
    correct: number;
    activity_id?: number;
    lesson_id?: number;
    type?: string;
  }>
> {
  return query(
    'select r.started, r.completed, r.items_complete complete, r.items_qty qty, sum(r2.is_correct) correct, \
    r2.activity_id, a.lesson_id, a.type \
  from results_summary r \
    join results r2 on r.user_id = r2.user_id and r.take_id = r2.take_id \
    join activities a on r2.activity_id = a.id \
  where r.site_id = ? and r.user_id = ? \
  group by r.started, r.completed, r.items_complete, r.items_qty, r2.activity_id, a.lesson_id, a.type \
  order by r.started desc \
  limit ?, ?',
    [siteId, uid, page * qtyPerpage, qtyPerpage]
  );
}

export async function getQtyResultsForUser(siteId: string, uid: string): Promise<number> {
  const res = await query<Array<{ qty: number }>>(
    'select count(s.user_id) qty from results_summary s where s.site_id = ? and s.user_id = ?',
    [siteId, uid]
  );

  if (res.length === 0) {
    return 0;
  }

  return res[0].qty;
}
