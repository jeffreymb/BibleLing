'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.createTable('student_groups', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      autoIncrement: true,
    },
    site_id: {
      type: 'string',
      length: 5,
      notNull: true,
    },
    name: {
      type: 'string',
      length: 50,
      notNull: true,
    },
    inactive: {
      type: 'boolean',
      defaultValue: false,
      notNull: true,
    },
  });

  await db.createTable('student_groups_students', {
    group_id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      foreignKey: {
        name: 'fk_student_groups_students',
        table: 'student_groups',
        mapping: 'id',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      },
    },
    user_id: {
      type: 'string',
      length: '50',
      notNull: true,
      primaryKey: true,
    },
  });
};

exports.down = async function (db) {
  await db.dropTable('student_groups_students');
  await db.dropTable('student_groups');
};

exports._meta = {
  version: 1,
};
