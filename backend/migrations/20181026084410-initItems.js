var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, next) {
  return db
    .createTable('content', {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
      },
      label: {
        type: 'string',
        length: 255,
      },
    })
    .then(() => {
      db.createTable('content_text', {
        content_id: {
          type: 'int',
          primaryKey: true,
          unsigned: true,
          foreignKey: {
            name: 'fk_content_text_content_id',
            table: 'content',
            mapping: 'id',
            rules: {
              onDelete: 'CASCADE',
              onUpdate: 'CASCADE',
            },
          },
        },
        order: {
          type: 'int',
          unsigned: true,
          primaryKey: true,
          notNull: true,
        },
        text: {
          type: 'string',
          length: '255',
        },
        form_id: {
          type: 'int',
          unsigned: true,
          notNull: false,
        },
      });

      db.createTable('items', {
        id: {
          type: 'int',
          primaryKey: true,
          autoIncrement: true,
          unsigned: true,
        },
        order: {
          type: 'int',
          unsigned: true,
        },
        page_id: {
          type: 'int',
          notNull: true,
          foreignKey: {
            name: 'fk_items_page_id',
            table: 'pages',
            mapping: 'id',
            rules: {
              onDelete: 'CASCADE',
              onUpdate: 'CASCADE',
            },
          },
        },
        content_id: {
          type: 'int',
          unsigned: true,
          notNull: false,
          foreignKey: {
            name: 'fk_items_content_id',
            table: 'content',
            mapping: 'id',
            rules: {
              onDelete: 'RESTRICT',
              onUpdate: 'RESTRICT',
            },
          },
        },
        config: {
          type: 'text',
        },
      });

      db.addColumn('pages', 'content_id', {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'fk_pages_content_id',
          table: 'content',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT',
          },
        },
      });
    });
};

exports.down = function (db) {
  return db
    .dropTable('content_text')
    .then(() => {
      return db.removeForeignKey('pages', 'fk_pages_content_id', { dropIndex: true }).then(() => {
        return db.removeColumn('pages', 'content_id');
      });
    })
    .then(() => {
      return db.dropTable('items');
    })
    .then(() => {
      return db.dropTable('content');
    });
};

exports._meta = {
  version: 1,
};
