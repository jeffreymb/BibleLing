const tnx = require('../lib/migrate_translation');

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return tnx.insertTranslations(db, { save: 'Save' }, 'en');
};

exports.down = function (db) {
  return tnx.deleteTranslations(db, { save: 'Save' }, 'en');
};

exports._meta = {
  version: 1,
};
