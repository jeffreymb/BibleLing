'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.createTable('page_assets', {
    page_id: {
      type: 'int',
      unsigned: false,
      primaryKey: true,
      foreignKey: {
        name: 'fk_page_assets_page_id',
        table: 'pages',
        mapping: 'id',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      },
    },
    asset_id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      foreignKey: {
        name: 'fk_page_assets_asset_id',
        table: 'assets',
        mapping: 'id',
        rules: {
          onDelete: 'RESTRICT',
          onUpdate: 'CASCADE',
        },
      },
    },
  });
};

exports.down = async function (db) {
  await db.dropTable('page_assets');
};

exports._meta = {
  version: 1,
};
