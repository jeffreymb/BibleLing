// eslint-disable-next-line @typescript-eslint/no-var-requires
const tnx = require('../lib/migrate_translation');

const translations = {
  login: 'Log In',
  logout: 'Log Out',
  logout_of: 'Log Out of "{0}"',
  cancel: 'Cancel',
  'page_title.login': 'Log In',
};

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  // noop
};

exports.up = function (db) {
  return tnx.insertTranslations(db, translations, 'en');
};

exports.down = function (db) {
  return txn.deleteTranslations(db, translations, 'en');
};

exports._meta = {
  version: 1,
};
