'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.runSql('ALTER TABLE results ADD COLUMN `site_id` VARCHAR(5) NULL DEFAULT NULL AFTER id');

  await db.runSql(
    'ALTER TABLE results_summary ADD COLUMN `site_id` VARCHAR(5) NULL DEFAULT NULL FIRST'
  );

  await db.runSql(
    'UPDATE results r \
      JOIN activities a ON a.id = r.activity_id \
      JOIN lessons l ON l.id = a.lesson_id \
      SET r.site_id = l.site_id'
  );

  await db.runSql(
    'UPDATE results_summary rs \
      JOIN results r on r.user_id = rs.user_id AND r.take_id = rs.take_id \
      SET rs.site_id = r.site_id'
  );

  await db.removeForeignKey('results', 'results_results_summary', {
    dropIndex: true,
  });

  await db.runSql(
    'ALTER TABLE results_summary DROP PRIMARY KEY, ADD PRIMARY KEY (`site_id`, `user_id`, `take_id`)'
  );

  await db.addForeignKey(
    'results',
    'results_summary',
    'results_results_summary',
    {
      site_id: 'site_id',
      user_id: 'user_id',
      take_id: 'take_id',
    },
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    }
  );
};

exports.down = async function (db) {
  await db.removeForeignKey('results', 'results_results_summary', {
    dropIndex: true,
  });

  await db.runSql(
    'ALTER TABLE results_summary DROP PRIMARY KEY, ADD PRIMARY KEY (`user_id`, `take_id`)'
  );

  await db.addForeignKey(
    'results',
    'results_summary',
    'results_results_summary',
    {
      user_id: 'user_id',
      take_id: 'take_id',
    },
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    }
  );

  await db.removeColumn('results_summary', 'site_id');

  await db.removeColumn('results', 'site_id');
};

exports._meta = {
  version: 1,
};
