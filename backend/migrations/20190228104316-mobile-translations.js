const tnx = require('../lib/migrate_translation');

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

let translations = {
    'mobile.download': 'Download',
    'mobile.cancel': 'Cancel',
    'mobile.app_offline': 'Please connect to the internet to download lessons.',
    'mobile.activity': 'Activity',
    'mobile.activity_download_prompt': 'This activity is not currently downloaded.\\nWould you like to download it?',
    'mobile.download_lessons': 'Download Lessons',
    'mobile.refresh_lessons': 'Reload List',
    'mobile.save_lessons': 'Download Lessons',
    'mobile.qty_activities': '({0}/{1} Activities)',
};

exports.up = function (db) {
    return tnx.insertTranslations(db, translations, 'en');
};

exports.down = function (db) {
    return tnx.deleteTranslations(db, translations, 'en');
};

exports._meta = {
    "version": 1
};
