'use strict';

var dbm;
var type;
var seed;
var fs = require('fs');
var path = require('path');
var Promise;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
    Promise = options.Promise;
};

exports.up = function (db) {
    var filePath = path.join(__dirname, 'sqls', '20181128122058-initSiteId-up.sql');
    return new Promise(function (resolve, reject) {
        fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
            if (err) return reject(err);

            resolve(data);
        });
    })
    .then(function (data) {
        const queries = data.split(';');
        for (let i = 0; i < queries.length; i++) {
            const q = queries[i].trim();
            if (q.length === 0) {
                continue;
            }
            db.runSql(q);
        }
    });
};

exports.down = function (db) {
    var filePath = path.join(__dirname, 'sqls', '20181128122058-initSiteId-down.sql');
    return new Promise(function (resolve, reject) {
        fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
            if (err) return reject(err);

            resolve(data);
        });
    })
    .then(function (data) {
        const queries = data.split(';');
        for (let i = 0; i < queries.length; i++) {
            const q = queries[i].trim();
            if (q.length === 0) {
                continue;
            }
            db.runSql(q);
        }
    });
};

exports._meta = {
    "version": 1
};
