'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.createTable('results_summary', {
    user_id: {
      type: 'string',
      length: 50,
      primaryKey: true,
    },
    take_id: {
      type: 'bigint',
      unsigned: true,
      primaryKey: true,
    },
    started: {
      type: 'bigint',
      unsigned: true,
    },
    completed: {
      type: 'bigint',
      unsigned: true,
    },
    items_qty: {
      type: 'int',
    },
    items_complete: {
      type: 'int',
    },
  });

  await db.runSql(
    'INSERT INTO results_summary \
      SELECT DISTINCT user_id, take_id, 0, 0, COUNT(DISTINCT i.id) total, COUNT(DISTINCT r.id) qty\
      FROM results r \
    JOIN pages p ON p.activity_id = r.activity_id\
    JOIN items i ON i.page_id = p.id\
    GROUP BY user_id, take_id;'
  );

  await db.addForeignKey(
    'results',
    'results_summary',
    'results_results_summary',
    {
      user_id: 'user_id',
      take_id: 'take_id',
    },
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    }
  );
};

exports.down = async function (db) {
  await db.removeForeignKey('results', 'results_results_summary', {
    dropIndex: true,
  });
  await db.dropTable('results_summary');
};

exports._meta = {
  version: 1,
};
