var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    return db.createTable('assets', {
        id: {
            type: 'int',
            unsigned: true,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: 'string',
            length: 100,
            notNull: true,
        },
        type: {
            type: 'string',
            length: 1,
            notNull: true,
        },
        caption: {
            type: 'string',
            length: 100,
            notNull: true,
        },
        credits: {
            type: 'string',
            length: 100,
        },
        license: {
            type: 'string',
            length: 100,
        },
    }).then(() => {
        db.createTable('item_assets', {
            item_id: {
                type: 'int',
                unsigned: true,
                primaryKey: true,
                foreignKey: {
                    name: 'fk_item_assets_item_id',
                    table: 'items',
                    mapping: 'id',
                    rules: {
                        onDelete: 'CASCADE',
                        onUpdate: 'CASCADE',
                    },
                },
            },
            asset_id: {
                type: 'int',
                unsigned: true,
                primaryKey: true,
                foreignKey: {
                    name: 'fk_item_assets_asset_id',
                    table: 'assets',
                    mapping: 'id',
                    rules: {
                        onDelete: 'RESTRICT',
                        onUpdate: 'CASCADE',
                    },
                },
            },
            order: {
                type: 'int',
                unsigned: true,
            },

        });
    });

};

exports.down = function (db) {
    return db.dropTable('item_assets').then(() => {
        db.dropTable('assets');
    })
};

exports._meta = {
    "version": 1
};
