var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, next) {
  db.createTable(
    'users',
    {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
      },
      email: {
        type: 'string',
        length: 255,
        unique: true,
        notNull: true,
      },
      password: {
        type: 'string',
        length: 255,
        notNull: true,
      },
    },
    () => {
      db.insert(
        'users',
        ['email', 'password'],
        [
          // password is "developer"
          'developer@example.com',
          '$2a$10$VOb8i.Svcn06MkJVbM.ezuYLXRrBRMBqTUdbUgM1dg8egt0uzANrW',
        ],
        next
      );
    }
  );
};

exports.down = function (db, cb) {
  db.dropTable('users', { ifExists: true }, cb);
};

exports._meta = {
  version: 1,
};
