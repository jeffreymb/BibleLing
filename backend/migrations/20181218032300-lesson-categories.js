'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return db
    .createTable('lesson_categories', {
      id: {
        type: 'int',
        unsigned: true,
        primaryKey: true,
        autoIncrement: true,
      },
      site_id: {
        type: 'string',
        length: 5,
        notNull: true,
      },
      order: {
        type: 'int',
        unsigned: true,
        notNull: true,
      },
      asset_id: {
        type: 'int',
        unsigned: true,
        foreignKey: {
          name: 'lesson_categories_asset_id_fk',
          table: 'assets',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'CASCADE',
          },
        },
      },
    })
    .then(() => {
      return db.addColumn('lessons', 'category_id', {
        type: 'int',
        unsigned: true,
        foreignKey: {
          name: 'lessons_lesson_category_id_fk',
          table: 'lesson_categories',
          mapping: 'id',
          rules: {
            onDelete: 'RESTRICT',
            onUpdate: 'CASCADE',
          },
        },
      });
    });
};

exports.down = function (db) {
  return db
    .removeForeignKey('lessons', 'lessons_lesson_category_id_fk', {
      dropIndex: true,
    })
    .then(() => {
      return db.removeColumn('lessons', 'category_id').then(() => {
        return db.dropTable('lesson_categories');
      });
    });
};

exports._meta = {
  version: 1,
};
