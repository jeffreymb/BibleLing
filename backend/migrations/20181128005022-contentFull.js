'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    return db.addColumn('content', 'text_full', {
        type: 'text',
        notNull: false,
    }).then(() => {
        db.runSql('drop view if exists content_joined');
    });
};

exports.down = function (db) {
    return db.removeColumn('content', 'text_full').then(() => {
        db.runSql('create or replace view content_joined as select `c`.`id` AS `id`,`c`.`label` AS `label`,group_concat(`ct`.`text` order by `ct`.`order` ASC separator \' \') AS `text` from (`content` `c` left join `content_text` `ct` on((`c`.`id` = `ct`.`content_id`))) group by `c`.`id` order by `c`.`label`');
    });
};

exports._meta = {
    "version": 1
};
