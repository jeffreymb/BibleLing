'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db, next) {
    db.createTable('languages', {
        lang: {
            type: 'string',
            length: 4,
            primaryKey: true,
        },
    }, () => {
        db.insert('languages', ['lang'], ['en'], next);
    });

    db.createTable('translations', {
        lang: {
            type: 'string',
            length: 4,
            primaryKey: true,
            foreignKey: {
                name: 'fk_string_id',
                table: 'languages',
                mapping: 'lang',
                rules: {
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
            },
        },
        string_id: {
            type: 'string',
            length: 255,
            primaryKey: true,
        },
        text: {
            type: 'text',
        }
    }, () => {
        db.insert('translations', ['lang', 'string_id', 'text'], [
            'en', '_language_en', 'English'
        ], next);
    });
};

exports.down = function (db, next) {
    db.dropTable('translations', () => {
        db.dropTable('languages', next);
    });
};

exports._meta = {
    "version": 1
};
