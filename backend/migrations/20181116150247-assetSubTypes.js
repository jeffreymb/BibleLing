'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    return db.addColumn('assets', 'subtype', {
        type: 'string',
        length: 1,
        notNull: false,
    }).then(() => {
        return db.runSql("update assets set type='d', subtype='i' where type='i'");
    });
};

exports.down = function (db) {
    return db.removeColumn('assets', 'subtype');
};

exports._meta = {
    "version": 1
};
