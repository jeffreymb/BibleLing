CREATE TABLE  `content_pages` (`id` VARCHAR (25) NOT NULL, `site_id` VARCHAR (5) NOT NULL, `order` INT(11) NOT NULL, `config` TEXT  , PRIMARY KEY (`id`, `site_id`)) ;
CREATE TABLE  `content_page_translations` (`id` VARCHAR (25) NOT NULL, `site_id` VARCHAR (5) NOT NULL, `lang` VARCHAR (4) NOT NULL, state INT(11) NOT NULL, `text` TEXT  , PRIMARY KEY (`id`, `site_id`, `lang`)) ;
ALTER TABLE `languages` ADD  INDEX `languages_site_lang` (`lang`, `site_id`);
ALTER TABLE `content_page_translations`
    ADD CONSTRAINT `content_page_translations_langauges_fk` FOREIGN KEY (`lang`,`site_id`) REFERENCES `languages` (`lang`,`site_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `content_page_translations_content_pages_fk` FOREIGN KEY (`id`,`site_id`) REFERENCES `content_pages` (`id`,`site_id`) ON DELETE CASCADE ON UPDATE CASCADE
;