ALTER TABLE users
    ADD COLUMN site_id VARCHAR(5) NOT NULL DEFAULT '_def_' AFTER id
;
ALTER TABLE `users`
    DROP INDEX `email`,
    ADD UNIQUE INDEX `email_site_id` (`email`, `site_id`)
;

ALTER TABLE settings
    ADD COLUMN site_id VARCHAR(5) NOT NULL DEFAULT '_def_' AFTER id,
    ADD COLUMN admin_only BIT(1) NOT NULL DEFAULT b'0' AFTER site_id,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (id, site_id)
;

ALTER TABLE languages
    ADD COLUMN site_id VARCHAR(5) DEFAULT '_def_' NOT NULL AFTER lang,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (lang, site_id)
;
ALTER TABLE translations
    ADD COLUMN site_id VARCHAR(5) DEFAULT '_def_' NOT NULL AFTER lang,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (lang, site_id, string_id);
;

ALTER TABLE lessons
    ADD COLUMN site_id VARCHAR(5) DEFAULT '_def_' NOT NULL AFTER id
;

CREATE TABLE asset_sites (
    asset_id INT UNSIGNED NOT NULL,
    site_id VARCHAR(5) NOT NULL,
    owner BIT(1) NOT NULL DEFAULT b'1',
    PRIMARY KEY (asset_id, site_id),
    CONSTRAINT fk_asset_sites_assets FOREIGN KEY (asset_id) REFERENCES assets (id) ON UPDATE CASCADE ON DELETE CASCADE
)
    ENGINE=InnoDB
;
INSERT INTO asset_sites (asset_id, site_id, owner)
SELECT id, '_def_', 1 FROM assets;

ALTER TABLE content
    ADD COLUMN site_id VARCHAR(5) DEFAULT '_def_' NOT NULL AFTER id
;
