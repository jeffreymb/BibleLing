ALTER TABLE users DROP COLUMN site_id;
ALTER TABLE `users`
    DROP INDEX `email_site_id`,
    ADD UNIQUE INDEX `email` (`email`)
;
ALTER TABLE settings
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (id),
    DROP COLUMN site_id,
    DROP COLUMN admin_only
;

ALTER TABLE languages DROP COLUMN site_id;
ALTER TABLE translations DROP COLUMN site_id;

ALTER TABLE lessons DROP COLUMN site_id;

DROP TABLE asset_sites;