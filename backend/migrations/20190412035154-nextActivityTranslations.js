const tnx = require('../lib/migrate_translation');

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

const translations = {
    'activity_complete': 'Activity Complete!',
    'next_activity': 'Start Next Activity <em>({activity})</em>',
    'repeat_activity': 'Repeat Activity <em>({activity})</em>'
};

exports.up = function (db) {
    return tnx.insertTranslations(db, translations, 'en');
};

exports.down = function (db) {
    return tnx.deleteTranslations(db, translations, 'en');
};

exports._meta = {
    "version": 1
};
