let dbm, type, seed;
/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, next) {
  db.createTable(
    'lessons',
    {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
      },
      order: {
        type: 'int',
        unsigned: true,
      },
    },
    next
  );

  db.createTable(
    'activities',
    {
      id: {
        type: 'int',
        primaryKey: true,
        unsigned: true,
        autoIncrement: true,
      },
      lesson_id: {
        type: 'int',
        notNull: true,
        unsigned: true,
        foreignKey: {
          name: 'fk_lesson_id',
          table: 'lessons',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        },
      },
      order: {
        type: 'int',
        unsigned: true,
      },
      type: {
        type: 'string',
        length: 25,
      },
      config: {
        type: 'text',
      },
    },
    next
  );

  db.createTable(
    'pages',
    {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
      },
      activity_id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        foreignKey: {
          name: 'fk_activity_id',
          table: 'activities',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        },
      },
      order: {
        type: 'int',
        unsigned: true,
      },
      config: {
        type: 'text',
      },
      note: {
        type: 'text',
      },
    },
    next
  );
};

exports.down = function (db, next) {
  db.dropTable('pages', () => {
    db.dropTable('activities', () => {
      db.dropTable('lessons', next);
    });
  });
};

exports._meta = {
  version: 1,
};
