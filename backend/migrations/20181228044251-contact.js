'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    return db.createTable('contact', {
        id: {
            type: 'int',
            unsigned: true,
            primaryKey: true,
            autoIncrement: true,
        },
        site_id: {
            type: 'string',
            length: 5,
            notNull: true,
        },
        content_page_id: {
            type: 'string',
            length: 25,
            foreignKey: {
                name: 'contact_content_page_id_fk',
                table: 'content_pages',
                mapping: 'id',
                rules: {
                    onDelete: 'SET NULL',
                    onUpdate: 'CASCADE',
                }
            },
        },
    })
        .then(() => {
            return db.createTable('contact_recipients', {
                id: {
                    type: 'int',
                    unsigned: true,
                    primaryKey: true,
                    autoIncrement: true,
                },
                contact_id: {
                    type: 'int',
                    unsigned: true,
                    foreignKey: {
                        name: 'contact_recipients_contact_id_fk',
                        table: 'contact',
                        mapping: 'id',
                        rules: {
                            onDelete: 'CASCADE',
                            onUpdate: 'CASCADE',
                        }
                    },
                },
                recipient: {
                    type: 'string',
                    length: 255,
                    notNull: false,
                },
                order: {
                    type: 'int',
                    unsigned: true,
                    notNull: true,
                },
            });
        })
        .then(() => {
            return db.addColumn('content_pages', 'public', {
                type: 'boolean',
                default: false,
                notNull: true,
            });
        });
};

exports.down = function (db) {
    return db.removeColumn('content_pages', 'public')
        .then(() => {
            return db.dropTable('contect_recipients');
        })
        .then(() => {
            return db.dropTable('contact');
        });
};

exports._meta = {
    "version": 1
};
