'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.createTable('results', {
    id: {
      type: 'bigint',
      unsigned: true,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: 'string',
      length: 50,
    },
    take_id: {
      type: 'bigint',
      unsigned: true,
    },
    activity_id: {
      type: 'int',
      unsigned: true,
      foreignKey: {
        name: 'fk_results_activity_id',
        table: 'activities',
        mapping: 'id',
        rules: {
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE',
        },
      },
    },
    item_id: {
      type: 'int',
      unsigned: true,
      foreignKey: {
        name: 'fk_results_item_id',
        table: 'items',
        mapping: 'id',
        rules: {
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE',
        },
      },
    },
    is_correct: {
      type: 'boolean',
    },
    content_id: {
      type: 'int',
      unsigned: true,
      foreignKey: {
        name: 'fk_results_content_id',
        table: 'content',
        mapping: 'id',
        rules: {
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE',
        },
      },
    },
    assets: {
      type: 'text',
    },
  });

  await db.addIndex('results', 'results_user_take', ['user_id', 'take_id'], false);
};

exports.down = async function (db) {
  await db.removeIndex('results', 'results_user_take');

  await db.dropTable('results');
};

exports._meta = {
  version: 1,
};
