'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db) {
  await db.runSql('ALTER TABLE assets MODIFY license VARCHAR(25)');

  await db.createTable('licenses', {
    id: {
      type: 'string',
      primaryKey: true,
      length: 25,
    },
    name: {
      type: 'string',
      length: 25,
    },
  });

  const rows = await new Promise((resolve) => {
    db.all(
      "SELECT site_id, settings from settings where id LIKE 'licenses'",
      [],
      (err, results) => {
        resolve(results);
      }
    );
  });

  let insertPromises = [];

  for (let i = 0; i < rows.length; i++) {
    const licenses = JSON.parse(rows[i].settings);

    for (let k = 0; k < licenses.length; k++) {
      insertPromises.push(
        db.runSql(
          "INSERT IGNORE INTO licenses (id, name) VALUES('" +
            licenses[k].id +
            "', '" +
            licenses[k].name +
            "')"
        )
      );
    }
  }

  await Promise.all(insertPromises);

  await db.runSql('SET FOREIGN_KEY_CHECKS=0');

  await db.addForeignKey(
    'assets',
    'licenses',
    'asset_licenses_fk',
    {
      license: 'id',
    },
    {
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    }
  );

  await db.runSql('SET FOREIGN_KEY_CHECKS=1');
};

exports.down = async function (db) {
  await db.removeForeignKey('assets', 'asset_licenses_fk', {
    dropInex: true,
  });

  await db.dropTable('licenses');
};

exports._meta = {
  version: 1,
};
