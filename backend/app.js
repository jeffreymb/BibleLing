import { appDebug } from './config/debug';
const debug = appDebug.extend('init_express');

import createError from 'http-errors';
import express from 'express';
import session from 'express-session';
import SessionStoreFactory from 'express-mysql-session';
import logger from './config/logger';
import path from 'path';
import passport from './config/passport';
import { sites } from './config/sites';
import cors from 'cors';

import index from './routes/index';
import auth from './routes/auth';
import lesson from './routes/lesson';
import lessonCategories from './routes/lesson_category';
import translation from './routes/translation';
import language from './routes/language';
import activity from './routes/activity';

import asset from './routes/asset';
import content from './routes/content';
import config from './routes/config';
import user from './routes/user';
import contentPage from './routes/content_page';
import contactRouter from './routes/contact';
import sitesRouter from './routes/sites';
import licenseRouter from './routes/license';
import resultRouter from './routes/result';
import studentGroupsRouter from './routes/studentGroups';

import rollbar from './config/rollbar';
import assetPath from './config/uploads';

debug('imports complete');

const SessionStore = SessionStoreFactory(session);

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('x-powered-by', false);
if (app.get('env') === 'production') {
  app.set('trust proxy', true);
}

logger(app);

debug('logger complete');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

if (app.get('env') !== 'production') {
  app.use(
    cors({
      origin: true,
      credentials: true,
      methods: ['GET', 'POST', 'DELETE'],
      allowedHeaders: ['Content-Type', 'Cache-Control', 'x-requested-with', 'authorization'],
    })
  );
}

app.use((req, res, next) => {
  if (sites[req.hostname] === undefined) {
    if (Object.keys(sites).length > 0) {
      next(createError(404, `Invalid host name '${req.hostname}'`));
      return;
    } else {
      req.siteId = '_def_';
    }
  } else {
    req.siteId = sites[req.hostname];
  }
  next();
});

app.use(
  session({
    store: new SessionStore({
      table: 'sessions',
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
    }),
    name: 'session',
    cookie: {
      // two days
      maxAge: 3600 * 48 * 1000,
      sameSite: 'strict',
      secure: 'auto',
    },
    resave: false,
    saveUninitialized: false,
    secret: process.env.SECRET,
    rolling: true,
  })
);

debug('session complete');

app.use(passport.initialize({ userProperty: 'adminUser' }));
app.use(passport.session());

debug('passport complete');

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/api/auth', auth);
app.use('/api/lesson', lesson);
app.use('/api/lesson_categories', lessonCategories);
app.use('/api/translation', translation);
app.use('/api/language', language);
app.use('/api/activity', activity);
app.use('/api/asset', asset);
app.use('/api/content', content);
app.use('/api/config', config);
app.use('/api/user', user);
app.use('/api/content_page', contentPage);
app.use('/api/contact', contactRouter);
app.use('/api/sites', sitesRouter);
app.use('/api/licenses', licenseRouter);
app.use('/api/result', resultRouter);
app.use('/api/student_groups', studentGroupsRouter);

app.use('/assets', express.static(assetPath));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

debug('routes complete');

const rollbarErrorHandler = rollbar.errorHandler();
app.use(function (err, req, res, next) {
  if (err.status && err.status === 404) {
    next(err);
  } else {
    console.log(err);
    rollbarErrorHandler(err, req, res, next);
  }
});

debug('rollbar complete');

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

debug('done');

export default app;
