To run in emulator:
- `cordova emulate android`

To access the on-device Sqlite database:
- `adb sheell`
- `run-as org.bibleling.hebrew.langapp sqlite3 databases/langapp.db`

