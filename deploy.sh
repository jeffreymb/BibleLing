cd /data/bibleling.org/app
docker-compose pull
docker-compose down
docker-compose up -d
docker-compose exec -T app node node_modules/.bin/db-migrate up -e main --config config/migrations.json